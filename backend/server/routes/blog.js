var express = require('express');
var router = express.Router();
var url = require('url');

const WPAPI = require('wpapi');
var async = require('async');

var userPwd = "wopin123456";
// var endPoint = 'https://wifi.h2popo.com/wp-json';
var endPoint = 'http://localhost/wp-json';

var Post = require('./../models/blog.js').Post;
var Comment = require('./../models/blog.js').Comment;
var History = require('./../models/blog.js').History;
var Follow = require('./../models/blog.js').Follow;
var BlogMsg = require('./../models/blog.js').BlogMsg;
var SysMsg = require('./../models/blog.js').SysMsg;

var User = require('./../models/users.js').UserSchema;

const wpAdmin = new WPAPI({
  endpoint: endPoint,
  username: 'admin',
  password: 'wopin123321'
});
/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', { title: 'Express' });
});


function checkHasPostRecord(postId, callback) {
  Post.findOne({ "id": postId }, function (err, doc) {
    if (err) { return next(err); }
    else {
      if (doc) {
        callback(doc);
      } else {
        var post = new Post({
          id: postId,
          userId: '5b66bbf64238af6a804aacaf',
          userName: 'admin',
          likes: [],
          comments: 0,
          collect: []
        });
        post.save(function (err, postNew) {
          if (err) { return next(err); }
          else {
            callback(postNew);
          }
        });
      }
    }
  });
}

function checkHasCommentRecord(id, callback) {
  Comment.findOne({ "id": id }, function (err, doc) {
    if (err) { return next(err); }
    else {
      if (doc) {
        callback(doc);
      } else {
        var comment = new Comment({
          id: id,
          likes: []
        });
        comment.save(function (err, commentNew) {
          if (err) { return next(err); }
          else {
            callback(commentNew);
          }
        });
      }
    }
  });
}


router.post('/collect', function (req, res, next) {

  const _id = req.cookies.userId;
  const postId = req.body.postId;

  checkHasPostRecord(postId, function (doc) {
    if (doc.collect.indexOf(_id) == -1) {
      doc.collect.push(_id);
    }
    doc.save(function (err1, doc1) {
      if (err1) { return next(err1); }
      else {
        res.json({
          status: "0",
          msg: '',
          result: doc1
        })
      }
    });
  })
});

router.post('/unCollect', function (req, res, next) {

  const _id = req.cookies.userId;
  const postId = req.body.postId;

  checkHasPostRecord(postId, function (doc) {
    var index = doc.collect.indexOf(_id);
    if (index > -1) {
      doc.collect.splice(index, 1);
    }
    doc.save(function (err1, doc1) {
      if (err1) { return next(err1); }
      else {
        res.json({
          status: "0",
          msg: '',
          result: doc1
        })
      }
    });
  });

});

router.post('/unLike', function (req, res, next) {

  const _id = req.cookies.userId;
  const postId = req.body.postId;

  checkHasPostRecord(postId, function (doc) {
    var index = doc.likes.indexOf(_id);
    if (index > -1) {
      doc.likes.splice(index, 1);
    }
    doc.save(function (err1, doc1) {
      if (err1) { return next(err1); }
      res.json({
        status: "0",
        msg: '',
        result: doc1
      })
    });

  });
});

router.post('/like', function (req, res, next) {

  const _id = req.cookies.userId;
  const postId = req.body.postId;

  checkHasPostRecord(postId, function (doc) {
    if (doc.likes.indexOf(_id) == -1) {
      doc.likes.push(_id);
    }
    doc.save(function (err1, doc1) {
      if (err1) { return next(err1); }
      res.json({
        status: "0",
        msg: '',
        result: doc1
      })
    });
  });
});


router.post('/likeComment', function (req, res, next) {

  const _id = req.cookies.userId;
  const id = req.body.id;

  checkHasCommentRecord(id, function (doc) {
    if (doc.likes.indexOf(_id) == -1) {
      doc.likes.push(_id);
    }
    doc.save(function (err1, doc1) {
      if (err1) { return next(err1); }
      else {
        res.json({
          status: "0",
          msg: '',
          result: doc1
        })
      }
    });
  })

});

router.post('/unLikeComment', function (req, res, next) {

  const _id = req.cookies.userId;
  const id = req.body.id;

  checkHasCommentRecord(id, function (doc) {
    var index = doc.likes.indexOf(_id);
    if (index > -1) {
      doc.likes.splice(index, 1);
    }
    doc.save(function (err1, doc1) {
      if (err1) { return next(err1); }
      else {
        res.json({
          status: "0",
          msg: '',
          result: doc1
        })
      }
    });
  })

});

router.get('/blogPostData/:postIds', function (req, res, next) {

  var postIds = req.params.postIds.split(",");
  Post.find({ "id": { $in: postIds } }, function (err, docs) {
    if (err) { return next(err); }
    else {
      if (docs) {
        res.json({
          status: "0",
          msg: '',
          result: docs
        })
      } else {
        res.json({
          status: "1",
          msg: ''
        })
      }
    }
  });
});

router.get('/blogCommentData/:ids', function (req, res, next) {

  var ids = req.params.postIds.split(",");
  Comment.findOne({ "id": { $in: ids } }, function (err, docs) {
    if (err) { return next(err); }
    else {
      if (docs) {
        res.json({
          status: "0",
          msg: '',
          result: docs
        })
      } else {
        res.json({
          status: "1",
          msg: ''
        })
      }
    }
  });
});

router.post('/newPost', function (req, res, next) {

  const _id = req.cookies.userId;
  const title = req.body.title;
  const content = req.body.content;

  const wp = new WPAPI({
    endpoint: endPoint,
    username: _id,
    password: userPwd
  });

  wp.posts().create({
    title: title,
    content: content,
    comment_status: "open",
    status: 'publish'
  }).then(function (response) {
    // console.log(response.id);
    res.json({
      status: "0",
      msg: '',
      result: response.id
    });

    User.findOne({ "_id": _id }, function (err, user) {
      if (user) {
        var post = new Post({
          id: response.id,
          userId: _id,
          userName: user.userName,
          likes: [],
          comments: 0,
          collect: [],
          read: 0
        });
        post.save()
      }
    });
  })
});


router.post('/newComment', function (req, res, next) {

  const _id = req.cookies.userId;
  const postId = req.body.postId;
  const content = req.body.content;
  const parent = req.body.parent || 0;

  var wp = new WPAPI({
    endpoint: endPoint,
    username: _id,
    password: userPwd
  });

  wp.comments().create({
    post: postId,
    content: content,
    parent: parent
  }).then(function (response) {
    // console.log(response);
    var dataUnit = parseComment(response);
    dataUnit["myLike"] = false;
    res.json({
      status: "0",
      msg: '',
      result: dataUnit
    });

    var comment = new Comment({
      id: response["id"],
      postId: postId,
      userId: _id,
      parent: parent,
      likes: []
    });
    comment.save()

    wpAdmin.comments().param('post', postId).get(function (err, comments) {
      Post.updateOne({ "id": postId }, { $set: { "comments": comments.length } }, function (err, doc) { });

      getPosts(_id, 1, 1, [postId], function (list) {
        var postThumbnail = "";
        var postTitle = "";
        if (list.length > 0) {
          postTitle = list[0].title;
          postThumbnail = list[0].featured_image;
        }
        var newMsgData = {
          "id": dataUnit.id,
          "post": dataUnit.post,
          "parent": dataUnit.parent,
          "author": dataUnit.author,
          "author_name": dataUnit.author_name,
          "avatar_URL": dataUnit.avatar_URL,
          "content": dataUnit.content,
          "date": dataUnit.date,
          "type": dataUnit.type,
          "postTitle": postTitle,
          "postThumbnail": postThumbnail
        }
        if (parent > 0) {
          comments.forEach((item) => {
            if (item.id == parent) {
              Comment.findOne({ "id": parent }, function (err, com) {
                if (com) {
                  BlogMsg.findOne({ "userId": com.userId }, function (err, blogM) {

                    if (blogM) {
                      blogM.newComment.push(newMsgData);
                      blogM.save();
                    } else {
                      var newDoc = new BlogMsg({ 'userId': com.userId, 'newComment': [newMsgData], 'time': 0 });
                      newDoc.save();
                    }
                  });
                }
              })
            }
          });
        }
        Post.findOne({ "id": postId }, function (err, doc) {
          if (doc) {
            if (_id != doc.userId) {
              BlogMsg.findOne({ "userId": doc.userId }, function (err, blogM) {
                if (blogM) {
                  blogM.newComment.push(newMsgData);
                  blogM.save();
                } else {
                  var newDoc = new BlogMsg({ 'userId': doc.userId, 'newComment': [newMsgData], 'time': 0 });
                  newDoc.save();
                }
              });
            }
          }
        });
      })
    })
  })
});

router.post('/deleteComment', function (req, res, next) {

  const _id = req.cookies.userId;
  const commentId = req.body.commentId;

  Comment.findOne({ "id": commentId }, function (err, doc) {
    if (doc && doc.userId == _id) {

      Comment.remove({ "id": commentId }, function (err, doc) {
        res.json({
          status: "0",
          msg: ''
        });
      });
      wpAdmin.comments().id(commentId).delete().then(function (response) {
        // console.log(response);
        wpAdmin.comments().param('post', doc.postId).get(function (err, comments) {
          Post.updateOne({ "id": doc.postId }, { $set: { "comments": comments.length } }, function (err, doc) { });
        });
      })
    } else {
      res.json({
        status: "0",
        msg: ''
      });
    }
  });
});

router.get('/post/:id', function (req, res, next) {

  const id = req.params.id;
  if (id) {
    const _id = req.cookies.userId;
    var queryObject = url.parse(req.url, true).query;
    getPosts(_id, 1, 1, [id], function (list) {
      res.json({
        status: "0",
        msg: '',
        result: list
      });
    })
  } else {
    res.json({
      status: "0",
      msg: ''
    });
  }
});

router.get('/posts', function (req, res, next) {

  const _id = req.cookies.userId;
  var queryObject = url.parse(req.url, true).query;
  getPosts(_id, queryObject.page, queryObject.num, null, function (list) {
    res.json({
      status: "0",
      msg: '',
      result: list
    });
  })
});

function parseComment(comment) {
  var icon = comment["author_url"] && comment["author_url"].length > 0 ? comment["author_url"] : comment["author_avatar_urls"]['96'];
  var dataUnit = {
    "id": comment["id"],
    "post": comment["post"],
    "parent": comment["parent"],
    "author": comment["author"],
    "author_name": comment["author_name"],
    "avatar_URL": icon,
    "content": comment["content"]["rendered"],
    "date": comment["date"],
    "type": comment["type"]
  };
  return dataUnit;
}

router.get('/postComments/:postId', function (req, res, next) {

  const _id = req.cookies.userId;
  const _postId = req.params.postId;

  wpAdmin.comments().param('post', _postId).get(function (err, comments) {
    // console.log(posts);

    var resList = [];
    for (var i = 0; i < comments.length; i++) {
      var comment = comments[i];
      resList.push(parseComment(comment));
    }

    async.each(resList, function (item, callback) {
      Comment.findOne({ "id": item.id }, function (err, doc) {

        if (err) { callback(err); }
        else {
          if (doc) {
            item.myLike = doc.likes.indexOf(_id) > -1;
            item.likes = doc.likes.length;
          } else {
            item.myLike = false;
            item.likes = 0;
          }
          callback(null, item) // item not use
        }
      });
    }, function (err) {
      if (err) {
        res.json({
          status: "1",
          msg: 'err'
        });
      } else {
        // console.log(resList);
        res.json({
          status: "0",
          msg: '',
          result: resList
        });
        History.findOne({ "userId": _id }, function (err, hist) {
          if (err) { return next(err); }
          else {
            if (hist) {
              var index = hist.history.indexOf(_postId);
              if (index > -1) {
                hist.history.splice(index, 1);
              }
              hist.history.push(_postId);
              hist.save();
            } else {
              var newHistory = new History({ userId: _id, history: [_postId] });
              newHistory.save();
            }
          }
        });
        Post.findOne({ "id": _postId }, function (err, postdoc) {
          if (err) { return next(err); }
          else {
            if (postdoc) {
              postdoc.read = (parseInt(postdoc.read) || 0) + 1;
              postdoc.save();
            }
          }
        });
      }
    });
  });
});

router.get('/hotPosts', function (req, res, next) {

  const _id = req.cookies.userId;
  var queryObject = url.parse(req.url, true).query;
  var num = queryObject.num * 1 || 0;
  var page = queryObject.page * 1 || 0;

  let postModel = Post.find({}).sort({ 'comments': -1 }).skip(num * (page - 1)).limit(num);

  postModel.exec(function (err, docs) {
    if (err) { return next(err); }
    else {
      if (docs) {
        var postIds = [];
        docs.forEach((item) => {
          postIds.push(item.id);
        });

        getPosts(_id, 1, num, postIds, function (list) {

          removeNotExistPost(postIds, list);

          // list.sort(function (a, b) { return b.comments - a.comments });
          var resList = [];
          for (var i = 0; i < postIds.length; i++) {
            var _curId = postIds[i];
            list.forEach(function (element) {
              if (element.id == _curId) {
                resList.push(element);
                return false;
              }
            }, this);
          }
          res.json({
            status: "0",
            msg: '',
            result: resList
          });
        })
      } else {
        res.json({
          status: "0",
          msg: '',
          result: []
        });
      }
    }
  })
});

router.get('/historyPosts', function (req, res, next) {

  const _id = req.cookies.userId;
  var queryObject = url.parse(req.url, true).query;

  History.findOne({ "userId": _id }, function (err, hist) {
    if (err) { return next(err); }
    else {
      if (hist) {
        getPosts(_id, queryObject.page, queryObject.num, hist.history, function (list) {
          res.json({
            status: "0",
            msg: '',
            result: list
          });
        })
      } else {
        res.json({
          status: "0",
          msg: '',
          result: []
        });
      }
    }
  });
});

router.get('/colletionPosts', function (req, res, next) {

  const _id = req.cookies.userId;
  var queryObject = url.parse(req.url, true).query;

  Post.find({ "collect": _id }, function (err, docs) {
    if (err) { return next(err); }
    else {
      if (docs) {
        var postIds = [];
        docs.forEach((item) => {
          postIds.push(item.id);
        });
        if (postIds.length == 0) {
          res.json({
            status: "0",
            msg: '',
            result: []
          });
        } else {
          getPosts(_id, queryObject.page, queryObject.num, postIds, function (list) {
            res.json({
              status: "0",
              msg: '',
              result: list
            });
          })
        }
      } else {
        res.json({
          status: "0",
          msg: '',
          result: []
        });
      }
    }
  });
});

router.get('/likedPosts', function (req, res, next) {

  const _id = req.cookies.userId;
  var queryObject = url.parse(req.url, true).query;
  Post.find({ "likes": _id }, function (err, docs) {
    if (err) { return next(err); }
    else {
      if (docs) {
        var postIds = [];
        docs.forEach((item) => {
          postIds.push(item.id);
        });
        if (postIds.length == 0) {
          res.json({
            status: "0",
            msg: '',
            result: []
          });
        } else {
          getPosts(_id, queryObject.page, queryObject.num, postIds, function (list) {
            res.json({
              status: "0",
              msg: '',
              result: list
            });
          })
        }
      } else {
        res.json({
          status: "0",
          msg: '',
          result: []
        });
      }
    }
  });
});

router.get('/followPosts', function (req, res, next) {

  const _id = req.cookies.userId;
  var queryObject = url.parse(req.url, true).query;
  Follow.find({ "follow": _id }, function (err, docs) {
    if (err) { return next(err); }
    else {
      if (docs) {
        var userIds = [];
        docs.forEach((item) => {
          userIds.push(item.userId);
        });
        Post.find({ "userId": { $in: userIds } }, function (err, docs) {
          if (err) { return next(err); }
          else {
            if (docs) {
              var postIds = [];
              docs.forEach((item) => {
                postIds.push(item.id);
              });

              if (postIds.length == 0) {
                res.json({
                  status: "0",
                  msg: '',
                  result: []
                });
              } else {
                getPosts(_id, queryObject.page, queryObject.num, postIds, function (list) {
                  res.json({
                    status: "0",
                    msg: '',
                    result: list
                  });
                })
              }
            } else {
              res.json({
                status: "0",
                msg: '',
                result: []
              });
            }
          }
        });
      } else {
        res.json({
          status: "0",
          msg: '',
          result: []
        });
      }
    }
  });
});
router.get('/myComments', function (req, res, next) {

  var queryObject = url.parse(req.url, true).query;
  const _id = req.cookies.userId;

  Comment.find({ "userId": _id }, function (err, docs) {
    if (docs) {
      var commentIds = [];
      var resReplyToMeIds = [];
      docs.forEach((item) => {
        commentIds.push(item.id);
      });

      Comment.find({ "parent": { $in: commentIds } }, function (err, comReplys) {
        comReplys.forEach((item) => {
          commentIds.push(item.id);
          resReplyToMeIds.push(item.id);
        });
        if (commentIds.length > 0) {
          wpAdmin.comments().param('include', commentIds).perPage(queryObject.num).page(queryObject.page).then(function (coms) {
            // console.log(coms);
            var resList = [];
            var resReplyToMeList = [];
            var postIds = [];
            for (var i = 0; i < coms.length; i++) {
              var comment = coms[i];
              var isReply = false
              for (var j = 0; j < resReplyToMeIds.length; j++) {
                if (resReplyToMeIds[j] == comment.id) {
                  isReply = true;
                  break;
                }
              }
              if (isReply) {
                resReplyToMeList.push(parseComment(comment));
              } else {
                resList.push(parseComment(comment));
              }
              postIds.push(comment.post);
            }
            getPosts(_id, 1, queryObject.num, postIds, function (list) {
              res.json({
                status: "0",
                msg: '',
                result: {
                  comments: resList,
                  commentsReplyMe: resReplyToMeList,
                  relatedPosts: list
                }
              });
            })
          });
        } else {
          res.json({
            status: "0",
            msg: '',
            result: {
              comments: [],
              commentsReplyMe: [],
              relatedPosts: []
            }
          })
        }
      });
    } else {
      res.json({
        status: "1",
        msg: ''
      })
    }
  });
});

router.get('/myPosts', function (req, res, next) {

  var queryObject = url.parse(req.url, true).query;

  var userId = queryObject.userId;
  if (!userId) {
    userId = req.cookies.userId;
  }
  Post.find({ "userId": userId }, function (err, docs) {
    if (err) { return next(err); }
    else {
      if (docs) {
        var postIds = [];
        docs.forEach((item) => {
          postIds.push(item.id);
        });
        if (postIds.length == 0) {
          res.json({
            status: "0",
            msg: '',
            result: []
          });
        } else {
          getPosts(userId, queryObject.page, queryObject.num, postIds, function (list) {
            res.json({
              status: "0",
              msg: '',
              result: list
            });
          })
        }
      } else {
        res.json({
          status: "0",
          msg: '',
          result: []
        });
      }
    }
  });
});

router.post('/deletePost', function (req, res, next) {

  const _id = req.cookies.userId;
  const postId = req.body.postId;

  const wp = new WPAPI({
    endpoint: endPoint,
    username: _id,
    password: userPwd
  });

  wp.posts().id(postId).delete().then(function (response) {
    // console.log(response.id);
    res.json({
      status: "0",
      msg: '',
      result: response.id
    });

    Post.remove({ "id": postId }, function (err, doc) {
    });
  })
});

function onlyUnique(value, index, self) {
  return self.indexOf(value) === index;
}

router.get('/fansList', function (req, res, next) {

  const _id = req.cookies.userId;
  Follow.findOne({ "userId": _id }, function (err, docs) {
    if (err) { return next(err); }
    else {
      if (docs) {
        var unique = docs.follow || [];

        User.find({ "_id": { $in: unique } }, function (err, users) {
          if (err) { return next(err); }
          else {
            if (users) {
              var resList = [];
              users.forEach((item) => {
                var dataUnit = {
                  "_id": item["_id"],
                  "userName": item["userName"],
                  "icon": item["icon"]
                };
                resList.push(dataUnit);
              });
              res.json({
                status: "0",
                msg: '',
                result: resList
              });
            }
          }
        });
      } else {
        res.json({
          status: "0",
          msg: '',
          result: []
        });
      }
    }
  });
});

router.get('/myFollowList', function (req, res, next) {

  const _id = req.cookies.userId;
  Follow.find({ "follow": _id }, function (err, docs) {
    if (err) { return next(err); }
    else {
      if (docs) {
        var userIds = [];
        docs.forEach((item) => {
          userIds.push(item.userId);
        });
        // var unique = userIds.filter(onlyUnique);
        User.find({ "_id": { $in: userIds } }, function (err, users) {

          if (err) { return next(err); }
          else {
            if (users) {
              var resList = [];
              users.forEach((item) => {
                var dataUnit = {
                  "_id": item["_id"],
                  "userName": item["userName"],
                  "icon": item["icon"]
                };
                resList.push(dataUnit);
              });
              res.json({
                status: "0",
                msg: '',
                result: resList
              });
            }
          }
        });
      } else {
        res.json({
          status: "0",
          msg: '',
          result: []
        });
      }
    }
  });
});

router.post('/searchPosts', function (req, res, next) {

  const _id = req.cookies.userId;
  const search = req.body.search;

  wpAdmin.posts().search(search).perPage(10).page(1).then(function (posts) {
    // console.log(posts);
    parsePosts(_id, posts, function (list) {
      res.json({
        status: "0",
        msg: '',
        result: list
      });
    });
  });
});



function getPosts(userId, page, num, postIds, callback) {
  if (postIds && postIds.length == 0) {
    callback([]);
  }
  wpAdmin.posts().categories(1).param('include', postIds).perPage(num).page(page).then(function (posts) {
    parsePosts(userId, posts, callback);
  });
}

function getSysPosts(userId, page, num, postIds, callback) {
  if (postIds && postIds.length == 0) {
    callback([]);
  }
  wpAdmin.posts().categories(22).param('include', postIds).perPage(num).page(page).then(function (posts) {
    parsePosts(userId, posts, callback);
  });
}

function parsePosts(userId, posts, callback) {
  var _id = userId;
  var resList = [];
  for (var i = 0; i < posts.length; i++) {
    var post = posts[i];
    var dataUnit = {
      "id": post["id"],
      "author": post["author"],
      "date": post["date"],
      "title": post["title"]["rendered"],
      "content": post["content"]["rendered"],
      "featured_media": post["featured_media"],
      "URL": post["link"]
    };
    resList.push(dataUnit);
  }

  async.each(resList, function (item, callback) {
    Post.findOne({ "id": item.id }, function (err, doc) {
      if (err) { callback(err); }
      else {
        var _userId = "admin";
        if (doc) {
          item.likes = doc.likes.length;
          item.stars = doc.collect.length;
          item.comments = doc.comments;
          item.read = doc.read;
          item.myLike = doc.likes.indexOf(_id) > -1;
          item.myStar = doc.collect.indexOf(_id) > -1;
          _userId = doc.userId;
        }
        else {
          item.likes = 0;
          item.stars = 0;
          item.comments = 0;
          item.read = 0;
          item.myLike = false;
          item.myStar = false;
        }
        wpAdmin.users().id(item.author).get(function (err, userData) {
          if (err) {
            callback(err)
          } else {
            var icon = userData["url"] && userData["url"].length > 0 ? userData["url"] : userData["avatar_urls"]['96'];

            var authorData = { "id": _userId, "name": userData.name, "avatar_URL": icon };
            item.author = authorData;
            if (item.featured_media == -1) {
              item.featured_image = GetFirstImages(item.content || "");
              // delete item["content"];
              callback(null)
            }
            else {
              wpAdmin.media().id(item.featured_media).get(function (err, mediaData) {

                if (err) {
                  callback(err)
                } else {
                  item.featured_image = mediaData.guid.rendered;
                  // delete item["content"];
                  callback(null) // item not use
                }
              });
            }
          }
        });
      }
    });
  }, function (err) {
    if (err) {
      callback([])
    } else {
      // console.log(resList);
      callback(resList)
    }
  });

}

function removeNotExistPost(postList, wpList) {
  var notExistPost = [];
  for (var i = 0; i < postList.length; i++) {
    var p_id = postList[i];
    var hasPost = false;
    for (var j = 0; j < wpList.length; j++) {
      var wp_id = wpList[j]["id"];
      if (wp_id == p_id) {
        hasPost = true;
        break;
      }
    }
    if (hasPost == false) {
      notExistPost.push(p_id);
    }
  }

  if (notExistPost.length > 0) {
    Post.remove({ "id": { $in: notExistPost } }, function (err, doc) {
    });
    console.log(notExistPost);
  }
}
router.get('/getFollows/:userId', function (req, res, next) {

  const _userId = req.params.userId;

  Follow.findOne({ "userId": _userId }, function (err, doc) {
    if (err) { return next(err); }
    else {
      if (doc) {
        res.json({
          status: "0",
          msg: '',
          result: doc
        });
      } else {
        res.json({
          status: "0",
          msg: ''
        });
      }
    }
  });
});

router.post('/follow', function (req, res, next) {

  const _id = req.cookies.userId;
  const userId = req.body.userId;

  Follow.findOne({ "userId": userId }, function (err, doc) {
    if (err) { return next(err); }
    else {
      if (doc) {
        if (doc.follow.indexOf(_id) == -1) {
          doc.follow.push(_id);
        }
        doc.save();
        res.json({
          status: "0",
          msg: ''
        });
      } else {
        var newFollow = new Follow({
          "userId": userId,
          "follow": [_id]
        });
        newFollow.save(function (err, info) {
          if (info) {
            res.json({
              status: "0",
              msg: ''
            });
          }
        })
      }
    }
  });
});

router.post('/unFollow', function (req, res, next) {

  const _id = req.cookies.userId;
  const userId = req.body.userId;

  Follow.findOne({ "userId": userId }, function (err, doc) {
    if (err) { return next(err); }
    else {
      if (doc) {
        var index = doc.follow.indexOf(_id);
        if (index > -1) {
          doc.follow.splice(index, 1);
        }
        doc.save();
        res.json({
          status: "0",
          msg: ''
        });
      } else {
        res.json({
          status: "0",
          msg: ''
        });
      }
    }
  });
});


router.get('/newBlogMessage', function (req, res, next) {

  const _id = req.cookies.userId;
  BlogMsg.findOne({ "userId": _id }, function (err, docs) {
    if (docs) {
      res.json({
        status: "0",
        msg: '',
        result: docs
      })
      docs.newComment = []
      docs.save()
    } else {
      res.json({
        status: "0",
        msg: ''
      });
    }
  });
});

router.get('/sysMessage', function (req, res, next) {

  const _id = req.cookies.userId;
  var queryObject = url.parse(req.url, true).query;

  getSysPosts(_id, 1, 20, null, function (list) {
    res.json({
      status: "0",
      msg: '',
      result: list
    });
    SysMsg.findOne({ "userId": _id }, function (err, doc) {
      if (doc) {
        doc.time = new Date().getTime();
        doc.save();
      } else {
        var sysMsg = new SysMsg({
          "userId": _id,
          "time": new Date().getTime()
        });
        sysMsg.save()
      }
    });
  })
});


router.get('/checkNewMessage', function (req, res, next) {
  const _id = req.cookies.userId;

  SysMsg.findOne({ "userId": _id }, function (err, doc) {
    var lastTime = 1000;  // can not be 0
    if (doc) {
      lastTime = doc.time;
    }
    wpAdmin.posts().categories(22).after(new Date(lastTime + (3600000 * 8))).then(function (posts) {

      BlogMsg.findOne({ "userId": _id }, function (err, docs) {
        var sysNum = posts.length || 0;
        if (docs) {
          var newCommentNum = docs.newComment.length || 0;
          var all = sysNum + newCommentNum;
          res.json({
            status: "0",
            msg: '',
            result: { 'count': all }
          })
        } else {
          res.json({
            status: "0",
            msg: '',
            result: { 'count': sysNum }
          })
        }
      });
    });
  });
});


function GetFirstImages(htmlText) {

  var imgReg = /<img.*?(?:>|\/>)/gi;
  //匹配src属性
  var srcReg = /src=[\'\"]?([^\'\"]*)[\'\"]?/i;
  var arr = htmlText.match(imgReg);
  if (arr) {
    var src = arr[0].match(srcReg);
    return src[1];
  } else {
    return "https://wifi.h2popo.com/wp-content/uploads/2018/09/dafault.jpg"
  }
}

module.exports = router;



// const wp = new WPAPI({
//   endpoint: 'https://wifi.h2popo.com/wp-json',
//   username: '5b49b5e020f46923ccc0158d',
//   password: userPwd
// });

  // wp.comments().create({
  //   post: 395,
  //   content: "content",
  //   parent: 0
  // }).then(function (response) {
  //   console.log(response);
  // })

  //   wp.comments().id(36).delete().then(function (response) {
  //   console.log(response);
  // })
  //  wp.comments().id(36).then(function (response) {
  //   console.log(response);
  // })

// wp.users().id(1).get(function (err, userData) {
//   console.log(userData);
// })
// wp.posts().id(382).delete().then(function (response) {
//     console.log(response);
//   })
// wp.users().me().update({
//   url: "http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLhKfKwcPX90dN5DXh6RibMibPGFia6LQ3jSSdQ4LziaIGub64YASQmWaUW7YJptOaOvTpmoq8xMTib4kw/132"
// }).then(function (err, data) {
//   if (err) {
//     // handle err
//     console.log(err);
//   } else {
//     console.log(JSON.stringify(data));
//   }
// });
// wp.comments().param('include', [82,98]).perPage(20).page(1).then(function (posts) {
//     console.log(posts);
//   });
// wp.posts().param('include', [82,43]).perPage(20).page(1).then(function (posts) {
//     console.log(posts);
//   });

// wp.posts().search("猪蹄").perPage(10).page(1).then(function (response) {
//       console.log(response);

//     });


  // wpAdmin.users().create({
  //       username: "5b9a470765a13d7519661364",
  //       name: "悠着...忧着",
  //       password: userPwd,
  //       email: '5b9a470765a13d7519661364@wopin.com',
  //       url:'http://qzapp.qlogo.cn/qzapp/1105931776/6CDE4A49C299412B0672B69F27793FE3/100'
  //   }).then(function (response) {
  //       console.log(response);
  //   })

// wp.categories().then(function (response) {
//       console.log(response);
//     });
// var timeDate = new Date().getTime() - 9999999999999;
//     wp.posts().categories(1).param('include', null).perPage(10).page(1).after(new Date(timeDate)).then(function (response) {
//       console.log(response.length);
//     });
// wp.posts().categories(1).perPage(20).page(1).then(function (response) {
//       console.log(response);

//     });

// wp.users().create({
//     // "title" and "content" are the only required properties
//     username: 'hyde',
//     password: '101010',
//     // Post will be created as a draft by default if a specific "status"
//     // is not specified
//     email: '8896819@163.com'
// }).then(function( response ) {
//     // "response" will hold all properties of your newly-created post,
//     // including the unique `id` the post was assigned on creation
//     console.log( response );
// })

// wp.posts().id(82).get(function (err, data) {
//   if (err) {
//     // handle err
//     // console.log( err );
//   } else {
//     console.log(JSON.stringify(data));
//   }
// });

// wpAdmin.users().id(2).get(function (err, data) {
//   if (err) {
//     // handle err
//     console.log( err );
//   } else {
//     console.log(JSON.stringify(data));
//   }
// });

// wpAdmin.media().id(80).get(function (err, data) {
//   if (err) {
//     // handle err
//     // console.log( err );
//   } else {
//     console.log(JSON.stringify(data));
//   }
// });

// wp.comments().param( 'post',395 ).get(function (err, data) {
//     if ( err ) {
//         console.log(err );
//     }
//     // do something with the returned posts
//     console.log(data );
// });

// wp.comments().get(function( err, data ) {
//     if ( err ) {
//         console.log(err );
//     }
//     // do something with the returned posts
//     console.log(data );
// });

// wp.posts().id( 2501 ).update({
//     // Update the title
//     title: 'A Better Title',
//     // Set the post live (assuming it was "draft" before)
//     status: 'publish'
// }).then(function( response ) {
//     console.log( response );
// })

