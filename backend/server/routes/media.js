var express = require('express');
var router = express.Router();
var url = require('url');
var fileSystem = require('fs');
var path = require('path');
var multer = require('multer');

/* GET home page. */
router.get('/:imageName', function (request, response, next) {

    // console.log(".......1...."+request.url);
    try {
        var filePath = path.join(__dirname, "../images/" + request.url);

        var stat = fileSystem.statSync(filePath);

        response.writeHead(200, {
            // 'Content-Type': 'audio/mpeg',
            'Content-Type': "application/octet-stream",
            'Content-Length': stat.size
        });

        var readStream = fileSystem.createReadStream(filePath);
        // We replaced all the event handlers with a simple call to readStream.pipe()
        readStream.pipe(response);
    }
    catch (err) {

        response.writeHead(404);
        response.end();
        console.log('it does not exist');
    }

});




var storage = multer.diskStorage({
    //设置上传后文件路径，uploads文件夹会自动创建。
    destination: function (req, file, cb) {
        cb(null, './images')
    },
    //给上传文件重命名，获取添加后缀名
    //  filename: function (req, file, cb) {
    //      var fileFormat = (file.originalname).split(".");
    //      cb(null, file.fieldname + '-' + Date.now() + "." + fileFormat[fileFormat.length - 1]);
    //  }
    filename: function (req, file, cb) {
        // var fileFormat = (file.originalname).split(".");
        cb(null, file.originalname);
    }
});
// 实例化上传模块(前端使用参数名为file)
var upload = multer({ storage: storage }).single('file');

router.post('/', function (req, res, next) {
    // var url = global.baseURL + req.url;

    upload(req, res, function (err) {
        if (err) {
            console.log('上传失败'+err);
        } else {
            // console.log('上传成功');
        }
        res.send({
            'states': '0',
            'url': "/images/"+req.file.originalname
        });
    });
});



module.exports = router;