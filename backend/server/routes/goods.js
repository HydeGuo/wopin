var axios = require('axios');
var express = require('express');
var router = express.Router(); // 拿到express框架的路由
var mongoose = require('mongoose');
var User = require('./../models/users.js').UserSchema;
var Goods = require('../models/goods').Produce;
var ExchangeGoods = require('../models/goods').ExchangeProduce;
var ExchangeOrder = require('./../models/orders').ExchangeOrder;
var ScoresOrder = require('./../models/orders').ScoresOrder;
var CrowdfundingOrder = require('./../models/orders').CrowdfundingOrder;
var fs = require('fs');


var _appId = "wxf42ec50449767feb";
var _mchId = "1513314731";
var _weChatKey = "50DK15GO1aGbESPQjBI1Wqubnq6ylFMS";
var WechatPayment = require('wechat-payment-node').default;
// import WechatPayment from 'wechat-payment-node';
let options = {
	appid: _appId,
	mch_id: _mchId,
	apiKey: _weChatKey, //微信商户平台API密钥
	notify_url: "http://wifi.h2popo.com:8081/goods/wechatpay",
	trade_type: 'APP',
	pfx: fs.readFileSync("./apiclient_cert.p12")
};
let wechatPaymentInstance = new WechatPayment(options);

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://127.0.0.1:27017/qingpaopao', { useMongoClient: true });  // 若是带账号密码的：'mongodb://root:123456@127.0.0.1:27017/dumall'

// 连接成功操作
mongoose.connection.on("connected", function () {
	console.log("MongoDB connected success.")
})

// 连接失败操作
mongoose.connection.on("error", function () {
	console.log("MongoDB connected fail.")
})

// 连接断开操作
mongoose.connection.on("disconnected", function () {
	console.log("MongoDB connected disconnected.")
})

var orderStatusArr = ["等待付款", "待发货", "已发货"];

var wx_preOrder_list = {}; // temp for preOrderId map order id

// 二级路由
// 查询商品列表数据
/* GET goods page. */
router.get('/list', function (req, res, next) {
	// res.send('hello,goods list');  // 测试路由，连接成功页面出现'hello,goods list'

	// express获取请求参数
	let page = parseInt(req.query.page);  // get请求数据拿到数据：res.param()
	let pageSize = parseInt(req.query.pageSize);
	let priceLevel = req.query.priceLevel;  // 传过来的价格区间
	let sort = req.query.sort;
	let skip = (page - 1) * pageSize; // 跳过的数据条数，(分页的公式).
	var priceGt = '', priceLte = '';
	let params = {};
	if (priceLevel != 'all') {   // 价格区间过滤功能
		switch (priceLevel) {
			case '0': priceGt = 0; priceLte = 100; break;
			case '1': priceGt = 100; priceLte = 500; break;
			case '2': priceGt = 500; priceLte = 1000; break;
			case '3': priceGt = 1000; priceLte = 5000; break;
		}
		params = {
			salePrice: {
				$gt: priceGt,
				$lte: priceLte
			}
		}
	}
	let goodsModel = Goods.find(params).skip(skip).limit(pageSize); // 先查询所有，skip(skip)跳过skip条数据，limit(pageSize)一页多少条数据.即分页功能实现
	goodsModel.sort({ 'salePrice': sort }); // 对价格排序功能

	goodsModel.exec(function (err, doc) {
		if (err) {
			res.json({
				status: '1',
				msg: err.message
			})
		} else {
			res.json({
				status: '0',
				msg: '',
				result: {
					count: doc.length,
					list: doc
				}
			})
		}
	})

});
// 启动express
// node server/bin/www 或 pm2方式 或 webstorm 等
// localhost:3000/goods/    // '/goods'是app.js中的一级路由，'/'是本页的二级路由


// 加入到购物车
// 是二级路由，一级路由在app.js
router.post("/addCart", function (req, res, next) {
	var userId = '100000077',
		productId = req.body.productId;  // post请求拿到res参数：req.body
	var User = require('../models/users.js');  // 引入user模型

	// 查询第一条:拿到用户信息
	User.findOne({
		userId: userId   // 查询条件
	}, function (err, userDoc) {
		if (err) {
			res.json({
				status: "1",
				msg: err.message
			})
		} else {
			console.log("userDoc" + userDoc);  // 用户数据
			if (userDoc) {
				let goodsItem = '';
				userDoc.cartList.forEach(function (item) {    // 遍历用户购物车，判断加入购物车的商品是否已经存在
					if (item.productId == productId) {
						goodsItem = item;
						item.productNum++; // 购物车这件商品数量+1
					}
				})
				if (goodsItem) {  // 若购物车商品已存在
					userDoc.save(function (err2, doc2) {
						if (err2) {
							res.json({
								status: "1",
								msg: err2.message
							})
						} else {
							res.json({
								status: '0',
								msg: '',
								result: 'suc'
							})
						}
					})
				} else {   // 若购物车商品不存在，就添加进去
					Goods.findOne({ productId: productId }, function (err1, doc) {  // 从商品列表页Goods查询点击加入购物车的那件商品信息
						if (err1) {
							res.json({
								status: "1",
								msg: err1.message
							})
						} else {
							if (doc) {
								doc.productNum = 1;   // 在Goods模型中添加属性，要去models/goods.js的Schema添加这两个属性。
								doc.checked = 1;
								userDoc.cartList.push(doc);  // 添加信息到用户购物车列表中
								userDoc.save(function (err2, doc2) {  // 保存数据库
									if (err2) {
										res.json({
											status: "1",
											msg: err2.message
										})
									} else {
										res.json({
											status: "0",
											msg: '',
											result: 'suc'
										})
									}
								})
							}
						}
					})
				}
			}
		}
	})
})


router.get('/exchangeList', function (req, res, next) {

	let goodsModel = ExchangeGoods.find();

	goodsModel.exec(function (err, doc) {
		if (err) {
			res.json({
				status: '1',
				msg: err.message
			})
		} else {
			res.json({
				status: '0',
				msg: '',
				result: {
					count: doc.length,
					list: doc
				}
			})
		}
	})

});

router.post("/orderStatusUpdate", function (req, res, next) {
	var orderId = req.body.orderId,
		status = req.body.status;

	wechatPaymentInstance.queryOrder({
		out_trade_no: orderId
	}).then(result => {
		console.log(result);
		if (result.trade_state == 'SUCCESS') {
			updateOrderStatue(req, res, next)
		} else {
			res.json({
				status: "1",
				msg: '支付状态异常'
			})
		}
	}).catch(err => {
		console.log(err);
		res.json({
			status: "1",
			msg: err
		})
	})
});

function updateOrderStatue(req, res, next) {
	var orderId = req.body.orderId,
		status = 1;

	var orderType = orderId.substr(0, 3);
	if (orderType == "622") {
		ExchangeOrder.updateOne({ "orderId": orderId }, { $set: { "orderStatus": orderStatusArr[status] } }, function (err, existingOrder) {
			if (err) { return next(err); }
			res.json({
				status: "0",
				msg: '',
				result: existingOrder
			})
		});
	}
	else if (orderType == "722") {
		ScoresOrder.updateOne({ "orderId": orderId }, { $set: { "orderStatus": orderStatusArr[status] } }, function (err, existingOrder) {
			if (err) { return next(err); }
			res.json({
				status: "0",
				msg: '',
				result: existingOrder
			})
		});
	}
	else if (orderType == "822") {
		CrowdfundingOrder.updateOne({ "orderId": orderId }, { $set: { "orderStatus": orderStatusArr[status] } }, function (err, existingOrder) {
			if (err) { return next(err); }
			res.json({
				status: "0",
				msg: '',
				result: existingOrder
			})
		});
	} else {
		res.json({
			status: "1",
			msg: '无效订单号'
		})
	}
}

function setPreOrderId(p_id, orderId) {
	wx_preOrder_list[p_id] = orderId;
}

router.post('/wechatpay', function (req, res, next) {
	// 处理商户业务逻辑

	// var parseString = require('xml2js').parseString;
	// parseString(req.body, function (err, result) {

	// 	console.dir(JSON.stringify(result));
	// 	if (result && result.result_code == 'SUCCESS') {

	// 		var orderId = wx_preOrder_list[result.prepay_id];
	// 		if (orderId) {
	// 			wechatPaymentInstance.queryOrder({
	// 				out_trade_no: orderId
	// 			}).then(result => {
	// 				console.log(result);
	// 				if (result.trade_state == 'SUCCESS') {
	// 					var status = 1;
	// 					var orderType = orderId.substr(0, 3);
	// 					if (orderType == "622") {
	// 						ExchangeOrder.updateOne({ "orderId": orderId }, { $set: { "orderStatus": orderStatusArr[status] } }, function (err, existingOrder) { });
	// 					}
	// 					else if (orderType == "722") {
	// 						ScoresOrder.updateOne({ "orderId": orderId }, { $set: { "orderStatus": orderStatusArr[status] } }, function (err, existingOrder) { });
	// 					}
	// 					else if (orderType == "822") {
	// 						CrowdfundingOrder.updateOne({ "orderId": orderId }, { $set: { "orderStatus": orderStatusArr[status] } }, function (err, existingOrder) { });
	// 					}
	// 				}
	// 			}).catch(err => {
	// 				console.log(err);
	// 			})
	// 		}
	// 	}	
	// });
	// res.status(200).send("<xml><return_code><![CDATA[SUCCESS]]></return_code><return_msg><![CDATA[OK]]></return_msg></xml>");
});

router.post("/deleteOrder", function (req, res, next) {
	var _id = req.cookies.userId;
	var orderId = req.body.orderId,
		status = req.body.status;
	var orderType = orderId.substr(0, 3);
	if (orderType == "622") {
		ExchangeOrder.remove({ "userId": _id, "orderId": orderId, "orderStatus": orderStatusArr[0] }, function (err, existingOrder) {
			if (err) { return next(err); }
			res.json({
				status: "0",
				msg: '',
				result: ""
			})
		});
	}
	else if (orderType == "722") {
		ScoresOrder.remove({ "userId": _id, "orderId": orderId, "orderStatus": orderStatusArr[0] }, function (err, existingOrder) {
			if (err) { return next(err); }
			res.json({
				status: "0",
				msg: '',
				result: ""
			})
		});
	}
	else if (orderType == "822") {
		CrowdfundingOrder.remove({ "userId": _id, "orderId": orderId, "orderStatus": orderStatusArr[0] }, function (err, existingOrder) {
			if (err) { return next(err); }
			res.json({
				status: "0",
				msg: '',
				result: ""
			})
		});
	} else {
		res.json({
			status: "1",
			msg: '无效订单号'
		})
	}

});
router.post('/payMentExchange', function (req, res, next) {
	// 前端传参：订单的地址id;订单最终的总金额
	var userId = req.cookies.userId,
		addressId = req.body.addressId,
		goodsId = req.body.goodsId,
		title = req.body.title,
		image = req.body.image,
		num = req.body.num,
		offerPrice = req.body.offerPrice,
		singlePrice = req.body.singlePrice
	User.findOne({ "_id": userId }, function (err, doc) {
		if (err) {
			res.json({
				status: '1',
				msg: err.message,
				result: ''
			})
		} else {
			var address = '', goodsList = [];

			//创建订单Id
			var platform = '622'; // 平台系统架构码
			var r1 = Math.floor(Math.random() * 10);
			var r2 = Math.floor(Math.random() * 10);

			var sysDate = new Date().Format('yyyyMMddhhmmss');  // 系统时间：年月日时分秒
			var orderId = platform + r1 + sysDate + r2;  // 21位

			doc.addressList.forEach((item) => {
				if (addressId == item.addressId) {
					address = item;
				}
			})

			var createDate = new Date().Format('yyyy-MM-dd hh:mm:ss');
			var order = new ExchangeOrder({
				orderId: orderId,           // 订单id
				userId: userId,
				title: title,
				image: image,
				goodsId: goodsId,
				address: address,
				num: num,
				offerPrice: offerPrice,
				singlePrice: singlePrice,
				orderStatus: orderStatusArr[0],
				createDate: createDate,
				createTime: new Date().getTime()
			});
			order.save(function (err_order, doc_order) {
				if (!err_order) {
					// doc.exchangeOrderList.push(orderId);
					doc.save(function (err1, doc1) {
						if (err1) {
							res.json({
								status: "1",
								msg: err.message,
								result: ''
							});
						} else {
							res.json({
								status: "0",
								msg: '',
								result: doc_order
							});
						}
					});
				}
			});
			updateGoodsStocks(goodsId);
		}
	})
});

router.get("/exchangeOrderList", function (req, res, next) {
	var userId = req.cookies.userId;

	ExchangeOrder.find({ userId: userId }, function (err, doc) {
		if (err) {
			res.json({
				status: '1',
				msg: err.message,
				result: ''
			});
		} else {
			if (doc) {
				res.json({
					status: '0',
					msg: '',
					result: doc
				})
			}
		}
	})
});


router.post("/exchangeOrderUpdate", function (req, res, next) {
	var orderId = req.body.orderId,
		expressReturnId = req.body.expressId;
	var expressReturnName = req.body.expressName || "";

	var infoUserName = req.body.infoUserName || "";
	var infoSex = req.body.infoSex || "";
	var infoPhone = req.body.infoPhone;
	var infoCupModel = req.body.infoCupModel || "";
	var infoCupColor = req.body.infoCupColor || "";
	var infoBuyTime = req.body.infoBuyTime || "";
	var infoUsage = req.body.infoUsage || "";

	var params = {
		"expressReturnId": expressReturnId,
		"expressReturnName": expressReturnName,
		"infoUserName": infoUserName,
		"infoSex": infoSex,
		"infoPhone": infoPhone,
		"infoCupModel": infoCupModel,
		"infoCupColor": infoCupColor,
		"infoBuyTime": infoBuyTime,
		"infoUsage": infoUsage
	}

	ExchangeOrder.updateOne({ "orderId": orderId }, { $set: params }, function (err, existingOrder) {
		if (err) { return next(err); }

		res.json({
			status: "0",
			msg: '',
			result: existingOrder
		})
	});
});

router.post("/deleteExchangeOrder", function (req, res, next) {
	var orderId = req.body.orderId;

	ExchangeOrder.remove({ "orderId": orderId }, function (err, existingOrder) {
		if (err) { return next(err); }
		res.json({
			status: "0",
			msg: '',
			result: ""
		})
	});
});


router.post('/payMentScores', function (req, res, next) {

	var userId = req.cookies.userId,
		addressId = req.body.addressId,
		goodsId = req.body.goodsId,
		title = req.body.title,
		image = req.body.image,
		num = req.body.num || 1,
		singlePrice = req.body.singlePrice
	User.findOne({ "_id": userId }, function (err, doc) {
		if (err) {
			res.json({
				status: '1',
				msg: err.message,
				result: ''
			})
		} else {
			var payScores = singlePrice * num;
			var address = '', goodsList = [];
			if (doc.scores < payScores) {
				res.json({
					status: "1",
					msg: "积分不足!",
					result: ''
				});
				return
			} else {
				doc.scores = doc.scores - payScores;
			}
			//创建订单Id
			var platform = '722';
			var r1 = Math.floor(Math.random() * 10);
			var r2 = Math.floor(Math.random() * 10);

			var sysDate = new Date().Format('yyyyMMddhhmmss');  // 系统时间：年月日时分秒
			var orderId = platform + r1 + sysDate + r2;  // 21位

			doc.addressList.forEach((item) => {
				if (addressId == item.addressId) {
					address = item;
				}
			})

			var createDate = new Date().Format('yyyy-MM-dd hh:mm:ss');
			var order = new ScoresOrder({
				orderId: orderId,           // 订单id
				userId: userId,
				title: title,
				image: image,
				goodsId: goodsId,
				address: address,
				num: num,
				singlePrice: singlePrice,
				orderStatus: '待发货',
				createDate: createDate,
				createTime: new Date().getTime()
			});
			order.save(function (err_order, doc_order) {
				if (!err_order) {
					// doc.exchangeOrderList.push(orderId);
					doc.save(function (err1, doc1) {
						if (err1) {
							res.json({
								status: "1",
								msg: err.message,
								result: ''
							});
						} else {
							res.json({
								status: "0",
								msg: '',
								result: doc1
							});
						}
					});
				}
			});

			updateGoodsStocks(goodsId);
		}
	})
});


router.get("/scoresOrderList", function (req, res, next) {
	var userId = req.cookies.userId;

	ScoresOrder.find({ userId: userId }, function (err, doc) {
		if (err) {
			res.json({
				status: '1',
				msg: err.message,
				result: ''
			});
		} else {
			if (doc) {
				res.json({
					status: '0',
					msg: '',
					result: doc
				})
			}
		}
	})
});

router.post("/deleteScoresOrder", function (req, res, next) {
	var orderId = req.body.orderId;
	ScoresOrder.remove({ "orderId": orderId }, function (err, existingOrder) {
		if (err) { return next(err); }
		res.json({
			status: "0",
			msg: '',
			result: ""
		})
	});
});




router.post('/payMentCrowdfunding', function (req, res, next) {
	// 前端传参：订单的地址id;订单最终的总金额
	var userId = req.cookies.userId,
		addressId = req.body.addressId,
		goodsId = req.body.goodsId,
		title = req.body.title,
		image = req.body.image,
		num = req.body.num,
		singlePrice = req.body.singlePrice
	User.findOne({ "_id": userId }, function (err, doc) {
		if (err) {
			res.json({
				status: '1',
				msg: err.message,
				result: ''
			})
		} else {
			var address = '', goodsList = [];

			//创建订单Id
			var platform = '822';
			var r1 = Math.floor(Math.random() * 10);
			var r2 = Math.floor(Math.random() * 10);

			var sysDate = new Date().Format('yyyyMMddhhmmss');  // 系统时间：年月日时分秒
			var orderId = platform + r1 + sysDate + r2;  // 21位

			doc.addressList.forEach((item) => {
				if (addressId == item.addressId) {
					address = item;
				}
			})

			var createDate = new Date().Format('yyyy-MM-dd hh:mm:ss');
			var order = new CrowdfundingOrder({
				orderId: orderId,           // 订单id
				userId: userId,
				title: title,
				image: image,
				goodsId: goodsId,
				address: address,
				num: num,
				singlePrice: singlePrice,
				orderStatus: orderStatusArr[0],
				createDate: createDate,
				createTime: new Date().getTime()
			});
			order.save(function (err_order, doc_order) {
				if (!err_order) {
					// doc.exchangeOrderList.push(orderId);
					doc.save(function (err1, doc1) {
						if (err1) {
							res.json({
								status: "1",
								msg: err.message,
								result: ''
							});
						} else {
							res.json({
								status: "0",
								msg: '',
								result: doc_order
							});
						}
					});
				}
			});

		}
	})
});


router.get("/crowdfundingOrderList", function (req, res, next) {
	var userId = req.cookies.userId;

	CrowdfundingOrder.find({ userId: userId }, function (err, doc) {
		if (err) {
			res.json({
				status: '1',
				msg: err.message,
				result: ''
			});
		} else {
			if (doc) {
				res.json({
					status: '0',
					msg: '',
					result: doc
				})
			}
		}
	})
});

router.post("/deleteCrowdfundingOrder", function (req, res, next) {
	var orderId = req.body.orderId;
	CrowdfundingOrder.remove({ "orderId": orderId }, function (err, existingOrder) {
		if (err) { return next(err); }
		res.json({
			status: "0",
			msg: '',
			result: ""
		})
	});
});

router.post("/crowdfundingOrderTotalMoney", function (req, res, next) {

	var goodsId = req.body.goodsId;
	CrowdfundingOrder.aggregate(
		[
			{ $match: { "goodsId": parseInt(goodsId), orderStatus: { $ne: orderStatusArr[0] } } },
			{ $group: { _id: null, "totalPrice": { $sum: "$singlePrice" } } }
		], function (err, result) {
			if (err) return next(err);
			res.json({
				status: '0',
				msg: '',
				result: result
			})
		}
	);

});
router.post("/crowdfundingOrderTotalPeople", function (req, res, next) {

	var goodsId = req.body.goodsId;
	CrowdfundingOrder.aggregate(
		[
			{ $match: { "goodsId": parseInt(goodsId), orderStatus: { $ne: orderStatusArr[0] } } },
			{ $group: { _id: null, "totalPeople": { $sum: 1 } } }
		], function (err, result) {
			if (err) return next(err);
			res.json({
				status: '0',
				msg: '',
				result: result
			})
		}
	);

});


function updateGoodsStocks(id) {
	const request1 = axios({
		method: 'post',
		url: "https://wifi.h2popo.com/wp-json/wc/v2/products/" + id + "?consumer_key=ck_7db681ac326401f87ec0a9ce564e45e2ef678f06&consumer_secret=cs_322de70d9956c27899eee79e97dedb6a403e8781",
		data: {},
		headers: { 'Content-Type': 'application/json' }
	}).then(function (response) {
		
		var stock = response.data["stock_quantity"];
		if (stock && stock > 0) {
			var newStock = stock - 1;
			const request2 = axios({
				method: 'post',
				url: "https://wifi.h2popo.com/wp-json/wc/v2/products/" + id + "?consumer_key=ck_7db681ac326401f87ec0a9ce564e45e2ef678f06&consumer_secret=cs_322de70d9956c27899eee79e97dedb6a403e8781",
				data: {
					"stock_quantity": newStock
				},
				headers: { 'Content-Type': 'application/json' }
			}).then(function (response) {

			})
		}
	})
}


module.exports = {
	router,
	setPreOrderId
};

