var express = require('express');
var router = express.Router();
let date = require('date-and-time');
const WPAPI = require('wpapi')

require('./../util/util');  // 引入时间格式化函数工具

var User = require('./../models/users.js').UserSchema;
var ExchangeOrder = require('./../models/orders').ExchangeOrder;
var ExchangeProduce = require('./../models/goods.js').ExchangeProduce;
var Drink = require('./../models/drink.js').Drink;
var Thirdparty = require('./../models/thirdparty.js');
var Devices = require('./../models/devices').Devices;
var EmailReset = require('./../models/users.js').EmailReset;


var endPoint = 'http://localhost/wp-json';
const wp = new WPAPI({
    endpoint: endPoint,
    username: 'admin',
    password: 'wopin123321'
});
var userPwd = "wopin123456";

/* GET users listing. */
router.get('/', function (req, res, next) {
    res.send('respond with a resource');
});

var phoneCodes = {};
var semdCodeTime = 60 * 1000

// 二级路由
// 登录接口
router.post("/login", function (req, res, next) {
    // 获取参数
    var param = {
        phone: req.body.phone,
        userPwd: req.body.userPwd
    }
    User.findOne(param, function (err, doc) {  // 根据用户名密码查找数据库
        if (err) {
            res.json({
                status: "1",
                msg: err.message
            })
        } else {
            if (doc) {
                res.cookie("userId", doc._id, {
                    path: '/',
                    maxAge: 1000 * 60 * 600
                });
                res.cookie("userName", doc.userName, {
                    path: '/',
                    maxAge: 1000 * 60 * 600
                });
                // req.session.user = doc;   要安装express session插件的
                res.json({
                    status: "0",
                    msg: '',
                    result: doc
                })
            } else {
                res.json({
                    status: "404",
                    msg: '用户或者密码错误.',
                    result: null
                })
            }
        }
    })
})
router.post("/emailLogin", function (req, res, next) {
    // 获取参数
    var param = {
        email: req.body.email,
        userPwd: req.body.userPwd
    }
    User.findOne(param, function (err, doc) {  // 根据用户名密码查找数据库
        if (err) {
            res.json({
                status: "1",
                msg: err.message
            })
        } else {
            if (doc) {
                res.cookie("userId", doc._id, {
                    path: '/',
                    maxAge: 1000 * 60 * 600
                });
                res.cookie("userName", doc.userName, {
                    path: '/',
                    maxAge: 1000 * 60 * 600
                });
                // req.session.user = doc;   要安装express session插件的
                res.json({
                    status: "0",
                    msg: '',
                    result: doc
                })
            } else {
                res.json({
                    status: "404",
                    msg: '用户或者密码错误.',
                    result: null
                })
            }
        }
    })
})
router.post("/thirdLogin", function (req, res, next) {
    // 获取参数
    var param = {
        thirdKey: req.body.key,
        thirdType: req.body.type
    }
    Thirdparty.findOne(param, function (err, doc) {
        if (err) {
            res.json({
                status: "1",
                msg: err.message
            })
        } else {
            if (doc) {
                User.findOne({ "_id": doc.uId }, function (err2, user) {  // 根据用户名密码查找数据库
                    if (err2) {
                        res.json({
                            status: "1",
                            msg: err.message
                        })
                    } else {
                        if (user) {
                            res.cookie("userId", user._id, {
                                path: '/',
                                maxAge: 1000 * 60 * 600
                            });
                            res.cookie("userName", user.userName, {
                                path: '/',
                                maxAge: 1000 * 60 * 600
                            });
                            res.json({
                                status: "0",
                                msg: '',
                                result: user
                            })
                        } else {
                            res.json({
                                status: "404",
                                msg: '没有用户资料.',
                                result: null
                            })
                        }
                    }
                });
            } else {
                res.json({
                    status: "1",
                    msg: "没有用户资料"
                })
            }
        }
    });
})

router.post("/emailRegister", function (req, res, next) {
    // Check for registration errors
    const email = req.body.email;
    const userName = req.body.userName;
    const userPwd = req.body.userPwd;

    // Return error if no email provided
    if (!email || !userPwd) {
        res.json({
            status: "422",
            msg: '请输入email 和密码.'
        });
        return;
    }
    User.findOne({ "email": email }, function (err, existingUser) {
        if (err) { return next(err); }

        if (existingUser) {
            res.json({
                status: "999",
                msg: '输入email已经注册.'
            });
            return;
        }
        var user = new User({
            email: email,
            icon: 'http://wifi.h2popo.com:8081/images/profileIcon.png',
            drinks: 0,
            scores: 0,
            userName: userName,
            userPwd: userPwd
        });
        user.save(function (err, user) {
            if (err) { return next(err); }
            else {
                res.json({
                    status: "0",
                    msg: '',
                    result: user
                })
                createBlogUser(user._id, userName);
            }
        });
    });


});

router.post("/register", function (req, res, next) {
    // Check for registration errors
    const phone = req.body.phone;
    const userName = req.body.userName;
    const userPwd = req.body.userPwd;
    const v_code = req.body.v_code;

    // Return error if no email provided
    if (!phone) {
        res.json({
            status: "422",
            msg: '请输入电话号码.'
        });
        return;
    }
    if (!userPwd) {
        res.json({
            status: "423",
            msg: '请输入密码.'
        });
        return;
    }
    if (checkCode(phone, v_code, res) == true) {
        User.findOne({ phone: phone }, function (err, existingUser) {
            if (err) { return next(err); }

            // If user is not unique, return error
            if (existingUser) {
                res.json({
                    status: "999",
                    msg: '用户号码已经注册.'
                });
                return;
            }
            var user = new User({
                phone: phone,
                icon: 'http://wifi.h2popo.com:8081/images/profileIcon.png',
                drinks: 0,
                scores: 0,
                userName: userName,
                userPwd: userPwd
            });
            user.save(function (err, user) {
                if (err) { return next(err); }
                else {
                    res.json({
                        status: "0",
                        msg: '',
                        result: user
                    })
                    createBlogUser(user._id, userName);
                }
            });
        });
    }

});

router.post("/thirdRegister", function (req, res, next) {
    // Check for registration errors
    const key = req.body.key;
    const type = req.body.type;
    const userName = req.body.userName;
    const uid = req.body.uid;

    Thirdparty.findOne({ thirdKey: key, thirdType: type }, function (err, doc) {
        if (err) { return next(err); }
        if (doc) {
            res.json({
                status: "0",
                msg: '',
                result: ''
            })
        } else {
            if (uid) {
                var thirdParty = new Thirdparty({
                    uId: uid,
                    thirdKey: key,
                    thirdType: type
                });
                thirdParty.save(function (err, third) {
                    if (err) { return next(err); }
                    res.json({
                        status: "0",
                        msg: '',
                        result: ''
                    })
                });
            } else {
                var user = new User({
                    userName: userName,
                    icon: 'http://wifi.h2popo.com:8081/images/profileIcon.png',
                    drinks: 0
                });
                user.save(function (err, user) {
                    if (err) { return next(err); }
                    var thirdParty = new Thirdparty({
                        uId: user._id,
                        thirdKey: key,
                        thirdType: type
                    });
                    thirdParty.save(function (err, third) {
                        if (err) { return next(err); }
                        res.json({
                            status: "0",
                            msg: '',
                            result: ''
                        })
                    });

                    createBlogUser(user._id, userName);
                });
            }
        }
    });
});

function createBlogUser(userId, name) {

    wp.users().create({
        username: userId,
        name: name,
        password: userPwd,
        email: userId + '@wopin.com'
    }).then(function (response) {
        console.log(response);
    })
}

router.post("/thirdBinding", function (req, res, next) {
    // Check for registration errors
    const _id = req.cookies.userId;
    const key = req.body.key;
    const type = req.body.type;

    Thirdparty.findOne({ thirdKey: key, thirdType: type }, function (err, doc) {
        if (err) { return next(err); }
        if (doc) {
            if (res.uid != _id) {
                res.json({
                    status: "1",
                    msg: '该第三方账号已经绑定其他账号',
                    result: ''
                })
            } else {
                res.json({
                    status: "0",
                    msg: '',
                    result: ''
                })
            }
        } else {
            var thirdParty = new Thirdparty({
                uId: _id,
                thirdKey: key,
                thirdType: type
            });
            thirdParty.save(function (err, third) {
                if (err) { return next(err); }
                res.json({
                    status: "0",
                    msg: '',
                    result: ''
                })
            });
        }
    });
});
router.post("/getThirdBinding", function (req, res, next) {
    // Check for registration errors
    const _id = req.cookies.userId;

    Thirdparty.find({ "uId": _id }, function (err, doc) {
        if (err) { return next(err); }
        res.json({
            status: "0",
            msg: '',
            result: doc
        })
    });
});

router.post("/resetEmailPassword", function (req, res, next) {
    // Check for registration errors
    const email = req.body.email;
    User.findOne({ "email": email }, function (err, existingUser) {
        if (existingUser) {
            var nodemailer = require('nodemailer');

            var transporter = nodemailer.createTransport({
                service: 'smtp.163.com',
                host: "smtp.163.com",
                secureConnection: true,
                port: 465,
                auth: {
                    user: 'wopin18927525270@163.com',
                    pass: '20160808wopin'
                }
            });

            var crypto = require('crypto');
            var md5 = crypto.createHash('md5');
            var token_str = email + (new Date().getTime())
            var token = md5.update(token_str).digest('hex');

            var mailOptions = {
                from: 'wopin18927525270@163.com',
                to: email,
                subject: '氢泡泡Qingpaopao登录密码重置',
                text: 'Click the link to reset password. 点击链接重置密码: http://wifi2.h2popo.com:8080/#/resetPassword?token=' + token
            };

            transporter.sendMail(mailOptions, function (error, info) {
                if (error) {
                    console.log(error);
                } else {
                    console.log('Email sent: ' + info.response);
                }
            });

            var emailReset = new EmailReset({
                "email": email,
                "token": token
            });
            emailReset.save();


            res.json({
                status: "0",
                msg: ''
            })
        }
        else {
            res.json({
                status: "1",
                msg: 'email 不存在'
            })
        }
    })
});


router.post("/checkPhone", function (req, res, next) {
    // Check for registration errors
    const phone = req.body.phone;
    const v_code = req.body.v_code;

    if (checkCode(phone, v_code, res) == true) {
        res.json({
            status: "0",
            msg: ''
        })
    }
});

router.post("/changePhone", function (req, res, next) {
    // Check for registration errors
    const _id = req.cookies.userId;
    const phone = req.body.phone;
    const v_code = req.body.v_code;

    if (checkCode(phone, v_code, res) == true) {
        User.findOne({ phone: phone }, function (err, doc) {
            if (err) { return next(err); }
            if (doc && doc._id != _id) {
                res.json({
                    status: '1',
                    msg: '电话号码已经绑定其他账号'
                })
            } else {
                User.findOne({ "_id": _id }, function (err, user) {
                    if (user) {
                        user.phone = phone;
                        user.save(function (err1, existingUser) {
                            if (err1) { return next(err1); }
                            res.json({
                                status: "0",
                                msg: '',
                                result: existingUser
                            })
                        });
                    }
                });
            }
        });
    }
});

router.post("/changeIcon", function (req, res, next) {
    // Check for registration errors
    const _id = req.cookies.userId;
    const icon = req.body.icon;

    User.findOne({ "_id": _id }, function (err, user) {
        if (user) {
            user.icon = icon;
            user.save(function (err1, existingUser) {
                if (err1) { return next(err1); }
                res.json({
                    status: "0",
                    msg: '',
                    result: existingUser
                })

                const wp = new WPAPI({
                    endpoint: endPoint,
                    username: _id,
                    password: userPwd
                });
                wp.users().me().update({
                    url: icon
                })
            });
        }
    });
});

router.post("/changeUserName", function (req, res, next) {
    // Check for registration errors
    const _id = req.cookies.userId;
    const userName = req.body.userName;
    User.findOne({ "_id": _id }, function (err, user) {
        if (user) {
            user.userName = userName;
            user.save(function (err1, existingUser) {
                if (err) { return next(err); }
                res.json({
                    status: "0",
                    msg: '',
                    result: existingUser
                })

                const wp = new WPAPI({
                    endpoint: endPoint,
                    username: _id,
                    password: userPwd
                });
                wp.users().me().update({
                    name: userName
                })
            });
        }
    });
});

router.post("/changePassword", function (req, res, next) {
    // Check for registration errors
    const phone = req.body.phone;
    const userPwd = req.body.userPwd;
    const v_code = req.body.v_code;

    if (!userPwd) {
        res.json({
            status: "423",
            msg: '请输入密码.'
        });
        return;
    }

    if (checkCode(phone, v_code, res) == true) {
        User.findOne({ phone: phone }, function (err, existingUser) {
            if (err) { return next(err); }
            if (!existingUser) {
                res.json({
                    status: "998",
                    msg: '用户号码未注册.'
                });
            } else {
                User.updateOne({ phone: phone }, { $set: { userPwd: userPwd } }, function (err, existingUser) { });
                res.json({
                    status: "0",
                    msg: ''
                })
            }
        });
    }
})

router.post("/changePasswordByEmail", function (req, res, next) {

    var userId = req.cookies.userId;   // 获取用户Id
    const userPwd = req.body.userPwd;

    if (!userPwd) {
        res.json({
            status: "423",
            msg: '请输入密码.'
        });
        return;
    }
    User.updateOne({ "_id": userId }, { $set: { userPwd: userPwd } }, function (err, existingUser) { });
    res.json({
        status: "0",
        msg: ''
    })
})

router.post("/changePasswordByToken", function (req, res, next) {
    // Check for registration errors
    const token = req.body.token;
    const password = req.body.password;

    if (!password) {
        res.json({
            status: "423",
            msg: '请输入密码.'
        });
        return;
    }
    EmailReset.findOne({ "token": token }, function (err, existingToken) {
        if (existingToken) {

            User.findOne({ 'email': existingToken.email }, function (err, existingUser) {

                if (existingUser) {
                    var crypto = require('crypto');
                    var md5 = crypto.createHash('md5');
                    var pwd = md5.update(password).digest('hex');
                    User.updateOne({ 'email': existingUser.email }, { $set: { userPwd: pwd } }, function (err, existingUser) { });
                    res.json({
                        status: "0",
                        msg: ''
                    })
                } else {
                    res.json({
                        status: "1",
                        msg: ''
                    });
                }
            });

        } else {
            res.json({
                status: "1",
                msg: 'token not exist.'
            });
        }
    })
})

// 登出接口
router.post("/logout", function (req, res, next) {
    res.cookie("userId", "", {
        path: "/",
        maxAge: -1  // 生命周期
    })
    res.json({
        status: "0",
        msg: '',
        result: ''
    })
})


// 校验是否登录接口
router.get("/checkLogin", function (req, res, next) {
    if (req.cookies.userId) {
        res.json({
            status: '0',
            msg: '',
            result: req.cookies.userName || ''
        });
    } else {
        res.json({
            status: '1',
            msg: '未登录',
            result: ''
        })
    }
})
// 发送校验接口
router.post("/sendVerifyCode", function (req, res, next) {

    const userPhone = req.body.phone;
    if (!userPhone) {
        res.json({
            status: "422",
            msg: '手机号码不能为空.'
        });
        return;
    }
    // if (phoneCodes[userPhone]) {
    //     var sendTime = phoneCodes[userPhone]["time"];
    //     if (new Date().getTime() - sendTime < semdCodeTime) {
    //         res.json({
    //             status: "426",
    //             msg: 'You send too fast.'
    //         });
    //         return
    //     }
    // }
    var code = randomCode(4);
    sendSMS(userPhone, code, res)
    phoneCodes[userPhone] = { code: code, time: new Date().getTime() };

})

router.post("/addOrUpdateACup", function (req, res, next) {
    const type = req.body.type;
    const uuid = req.body.uuid;
    const name = req.body.name;
    const address = req.body.address;
    const add = req.body.add;
    var userId = req.cookies.userId;   // 获取用户Id

    Devices.findOne({ "uuid": uuid }, function (err, doc) {
        if (err) { return next(err); }
        else {
            var sysDate = date.format(new Date(), 'YYYY-MM-DD H:mm');
            if (doc) {
                if (add == false) {
                    Devices.updateOne({ "uuid": uuid }, { $set: { "name": name } }, function (err, doc1) {
                        if (err) { return next(err); }
                        addScores(userId, 0, res)
                    });
                } else {
                    var pramas = { "userId": userId, "registerTime": sysDate };
                    if (address && address.length > 0) {
                        pramas["address"] = address
                    }
                    Devices.updateOne({ "uuid": uuid }, { $set: pramas }, function (err, doc1) {
                        if (err) { return next(err); }
                        addScores(userId, 0, res)
                    });
                }
            } else {
                var newDevice = new Devices({
                    "userId": userId,
                    "name": name,
                    "type": type,
                    "address": address,
                    "firstRegisterTime": sysDate,
                    "registerTime": sysDate,
                    "uuid": uuid,
                    "produceScores": 1000
                });
                // console.log(newDrink.target+".............");
                newDevice.save(function (err, docNew) {
                    if (err) { return next(err); }
                    addScores(userId, 1000, res)
                });
            }
        }
    });
})
router.post("/deleteACup", function (req, res, next) {
    const uuid = req.body.uuid;
    var userId = req.cookies.userId;   // 获取用户Id
    Devices.findOne({ "uuid": uuid }, function (err, doc) {
        if (err) { return next(err); }
        else {
            if (doc) {
                Devices.updateOne({ "uuid": uuid }, { $set: { "userId": "" } }, function (err, doc1) {
                    if (err) { return next(err); }
                    res.json({
                        status: '0',
                        msg: ''
                    });
                });
            } else {
                res.json({
                    status: '0',
                    msg: ''
                });
            }
        }
    });
})

router.post("/updateCupColor", function (req, res, next) {

    const uuid = req.body.uuid;
    const color = req.body.color;
    Devices.findOne({ "uuid": uuid }, function (err, doc) {
        if (err) { return next(err); }
        else {
            if (doc) {
                var pramas = { "color": color };
                Devices.updateOne({ "uuid": uuid }, { $set: pramas }, function (err, doc1) {
                    if (err) { return next(err); }
                    res.json({
                        status: '0',
                        msg: ''
                    });
                });
            } else {
                res.json({
                    status: '0',
                    msg: ''
                });
            }
        }
    });
})
router.get("/cupList", function (req, res, next) {

    var userId = req.cookies.userId;   // 获取用户Id
    Devices.find({ "userId": userId }, function (err, doc) {
        if (err) { return next(err); }
        else {
            res.json({
                status: "0",
                msg: '',
                result: doc
            })
        }
    });
})
router.post("/attendance", function (req, res, next) {

    var userId = req.cookies.userId;
    var sysDate = new Date().Format('yyyyMMdd');
    User.findOne({ "_id": userId }, function (err, doc) {
        if (err) {
            res.json({
                status: '1',
                msg: err.message,
                result: ''
            });
        } else {
            if (doc) {
                if (doc.lastAttendance != sysDate) {
                    if (!doc.scores) {
                        doc.scores = 0;
                    }
                    doc.scores = (parseInt(doc.scores) || 0) + 1;
                    doc.lastAttendance = sysDate;
                    doc.save(function (err1, doc1) {
                        if (err1) { return next(err1); }
                        res.json({
                            status: "0",
                            msg: '',
                            result: doc1
                        })
                    });
                }
                else {
                    res.json({
                        status: '0',
                        msg: "",
                        result: doc
                    });
                }
            }
        }
    })
})

router.post("/drink", function (req, res, next) {
    const uuid = req.body.uuid;
    const target = req.body.target;
    var userId = req.cookies.userId;
    var d_time = req.cookies.drinkTime; //'yyyy-MM-dd hh:mm'

    Devices.findOne({ "uuid": uuid }, function (err, devicesItem) {
        if (err) { return next(err); }
        else {
            if (devicesItem) {
                if (devicesItem.userId && devicesItem.userId.length > 0) {
                    doDrink(devicesItem.userId, [uuid], null, d_time, function () {
                        res.json({
                            status: '0',
                            msg: ''
                        });
                    })
                } else {
                    res.json({
                        status: '0',
                        msg: ''
                    });
                }
            } else {
                res.json({
                    status: '1',
                    msg: '无法识别的杯子'
                });
                // Devices.find({ "userId": userId }, function (err, cups) {
                //     if (cups) {
                //         var uuids = []
                //         for (var i = 0; i < cups.length; i++) {
                //             if (cups[i].type == "BLE") {    // WIFI CUP SEND BY  ITSELF
                //                 uuids.push(cups[i].uuid);
                //             }
                //         }
                //         doDrink(userId, uuids, target, d_time, function () {
                //             res.json({
                //                 status: '0',
                //                 msg: '',
                //                 result: { "count": cups.length }
                //             });
                //         });
                //     }
                // });
            }
        }
    });
})

function doDrink(userId, uuids, target, d_time, callBack) {

    var target = target || 8;
    var sysDate = new Date().Format('yyyyMMdd');
    var hasScore = false
    var nowDrinkTime = d_time || date.format(new Date(), 'YYYY-MM-DD H:mm');
    Drink.findOne({ "userId": userId, date: sysDate }, function (err, drinkItem) {
        if (err) { return next(err); }
        if (drinkItem) {
            var lastDrinkTime = drinkItem.drinks[drinkItem.drinks.length - 1].time;
            var lastDrinkTimeArr = lastDrinkTime.split(" ")[1].split(":");
            var nowDrinkTimeArr = nowDrinkTime.split(" ")[1].split(":");
            if (checkTimeInRange(Number(lastDrinkTimeArr[0]), Number(lastDrinkTimeArr[1])) != checkTimeInRange(Number(nowDrinkTimeArr[0]), Number(nowDrinkTimeArr[1]))) {
                hasScore = true;
            }
            for (var i = 0; i < uuids.length; i++) {
                drinkItem.drinks.push({ "time": nowDrinkTime });
            }
            drinkItem.save(function (err, doc3) {
                addUserScores(userId, uuids, hasScore, callBack);
            });
        } else {
            hasScore = true;
            var newDrink = new Drink({
                "userId": userId,
                "date": sysDate,
                "target": target,
                "drinks": [{ "time": nowDrinkTime }]
            });
            for (var i = 1; i < uuids.length; i++) {
                newDrink.drinks.push({ "time": nowDrinkTime });
            }
            // console.log(newDrink.target+".............");
            newDrink.save(function (err, doc) {
                addUserScores(userId, uuids, hasScore, callBack);
            });
        }

    });
}

function addUserScores(userId, uuids, hasScore, callBack) {

    var addScores = 0
    if (hasScore == true) {
        var sysDateH = date.format(new Date(), 'H');
        var sysDateM = date.format(new Date(), 'mm');
        var scoresIndex = checkTimeInRange(Number(sysDateH), Number(sysDateM));
        addScores = scores_for_drink[scoresIndex];
    }
    User.findOne({ "_id": userId }, function (err, user) {
        if (user) {
            var num = uuids.length
            user.scores += addScores * num;
            user.drinks += num;
            user.save(function (err1, doc1) {
                if (addScores > 0) {
                    for (var i = 0; i < uuids.length; i++) {
                        addProduceScores(uuids[i], addScores);
                    }
                }
                callBack();
            });
        }
    });
}
function addProduceScores(uuid, addScores) {
    Devices.findOne({ "uuid": uuid }, function (err, devicesItem) {
        if (devicesItem) {
            if (devicesItem.userId && devicesItem.userId.length > 0) {
                devicesItem.produceScores += addScores;
                devicesItem.save(function (err, doc) { });
            }
        }
    });
}


var scores_for_drink = [1, 1, 2, 2, 1, 2, 1, 2, 1, 2, 2, 2];
function checkTimeInRange(H, m) {
    if (H < 3) {
        return 0
    } else if (H < 6 || (H == 6 && m < 30)) {
        return 1
    } else if (H < 7) {
        return 2
    } else if (H < 10) {
        return 3
    } else if (H < 11) {
        return 4
    } else if (H < 12) {
        return 5
    } else if (H < 13 || (H == 13 && m < 30)) {
        return 6
    } else if (H < 15 || (H == 15 && m < 30)) {
        return 7
    } else if (H < 17 || (H == 17 && m < 30)) {
        return 8
    } else if (H < 20 || (H == 20 && m < 30)) {
        return 9
    } else if (H < 22) {
        return 10
    } else {
        return 11
    }
}

router.post("/getTodayDrinkList", function (req, res, next) {

    var _id = req.cookies.userId;
    var sysDate = new Date().Format('yyyyMMdd');
    Drink.findOne({ userId: _id, date: sysDate }, function (err, doc) {
        if (err) { return next(err); }
        if (doc) {
            res.json({
                status: "0",
                msg: '',
                result: doc
            })
        } else {
            res.json({
                status: "0",
                msg: ''
            })
        }
    });
});

router.post("/getDrinkList", function (req, res, next) {

    var userId = req.cookies.userId;   // 获取用户Id
    let drinkModel = Drink.find({ userId: userId }).sort({ 'date': -1 }).limit(31);
    drinkModel.exec(function (err, doc) {
        if (err) {
            res.json({
                status: '1',
                msg: err.message
            })
        } else {
            res.json({
                status: '0',
                msg: '',
                result: doc
            })
        }
    })
})


router.get("/getUserData", function (req, res, next) {
    if (req.cookies && req.cookies.userId) {
        var userId = req.cookies.userId;
        User.findOne({ "_id": userId }, function (err, user) {
            if (err) { return next(err); }
            else {
                res.json({
                    status: "0",
                    msg: "",
                    result: user
                });
            }
        });
    } else {
        res.json({
            status: "0",
            msg: "当前用户不存在"
        });
    }
});

// 查询购物车商品数量
router.get("/getCartCount", function (req, res, next) {
    if (req.cookies && req.cookies.userId) {
        console.log("userId:" + req.cookies.userId);
        var userId = req.cookies.userId;
        User.findOne({ "_id": userId }, function (err, doc) {
            if (err) {
                res.json({
                    code: 0,
                    msg: err.message
                });
            } else {
                var cartList = doc.cartList;
                var cartCount = 0;
                cartList.map(function (item) {
                    cartCount += parseFloat(item.productNum);
                });
                res.json({
                    status: "0",
                    msg: "",
                    result: cartCount
                });
            }
        });
    } else {
        res.json({
            status: "0",
            msg: "当前用户不存在"
        });
    }
});


// 查询当前用户的购物车数据
router.get('/cartList', function (req, res, next) {
    var userId = req.cookies.userId;   // 获取用户Id
    User.findOne({ "_id": userId }, function (err, doc) {
        if (err) {
            res.json({
                status: '1',
                msg: err.message,
                result: ''
            });
        } else {
            if (doc) {
                res.json({
                    status: '0',
                    msg: '',
                    result: doc.cartList
                })
            }
        }
    })
})



// 地址列表页面 **********************************************
// 查询用户地址接口
router.get("/addressList", function (req, res, next) {
    var userId = req.cookies.userId;
    User.findOne({ "_id": userId }, function (err, doc) {
        if (err) {
            res.json({
                status: '1',
                msg: err.message,
                result: ''
            })
        } else {
            res.json({
                status: '0',
                msg: '',
                result: doc.addressList
            })
        }
    })
})


//设置默认地址接口
router.post("/setDefaultAddress", function (req, res, next) {
    var userId = req.cookies.userId,
        addressId = req.body.addressId;
    if (!addressId) {
        res.json({
            status: '1003',
            msg: 'addressId is null',
            result: ''
        });
    } else {
        User.findOne({ "_id": userId }, function (err, doc) {
            if (err) {
                res.json({
                    status: '1',
                    msg: err.message,
                    result: ''
                });
            } else {
                var addressList = doc.addressList;
                addressList.forEach((item) => {
                    if (item.addressId == addressId) {
                        item.isDefault = true;
                    } else {
                        item.isDefault = false;
                    }
                });

                doc.save(function (err1, doc1) {
                    if (err) {
                        res.json({
                            status: '1',
                            msg: err.message,
                            result: ''
                        });
                    } else {
                        res.json({
                            status: '0',
                            msg: '',
                            result: doc
                        });
                    }
                })
            }
        });
    }
});

//删除地址接口
router.post("/delAddress", function (req, res, next) {
    var userId = req.cookies.userId, addressId = req.body.addressId;

    User.update({
        "_id": userId
    }, {
            $pull: {
                'addressList': {
                    'addressId': addressId
                }
            }
        }, function (err, doc) {
            if (err) {
                res.json({
                    status: '1',
                    msg: err.message,
                    result: ''
                });
            } else {
                res.json({
                    status: '0',
                    msg: '',
                    result: ''
                });
            }
        });
});
//添加地址接口
router.post("/addOrUpdateAddress", function (req, res, next) {


    const addressId = req.body.addressId;
    const userName = req.body.userName;
    const address1 = req.body.address1;
    const address2 = req.body.address2;
    const tel = req.body.tel;
    const isDefault = req.body.isDefault;

    var userId = req.cookies.userId;
    User.findOne({ "_id": userId }, function (err, doc) {
        if (err) {
            res.json({
                status: '1',
                msg: err.message,
                result: ''
            })
        } else {
            var address = null;
            doc.addressList.forEach((item) => {
                if (addressId == item.addressId) {
                    address = item;
                }
            })
            if (address == null) {
                address = {
                    addressId: addressId,
                    userName: userName,
                    address1: address1,
                    address2: address2,
                    tel: tel,
                    isDefault: isDefault
                }
                doc.addressList.push(address);
            } else {
                address.userName = userName;
                address.address1 = address1;
                address.address2 = address2;
                address.tel = tel;
                address.isDefault = isDefault;
            }
            doc.save(function (err1, doc1) {
                if (err1) {
                    res.json({
                        status: "1",
                        msg: err.message,
                        result: ''
                    });
                } else {
                    res.json({
                        status: "0",
                        msg: '',
                        result: doc
                    });
                }
            });
        }
    })

});

// 创建订单页面 **********************************************
// 创建订单功能
// router.post('/payMent', function(req,res,next){
//     // 前端传参：订单的地址id;订单最终的总金额
//     var userId = req.cookies.userId,
//         addressId = req.body.addressId,
//         goodId = req.body.goodId,
//         goodNum = req.body.goodNum,
//         orderTotal = req.body.orderTotal;
//     User.findOne({userId:userId}, function(err,doc){
//         if(err){
//             res.json({
//                 status:'1',
//                 msg:err.message,
//                 result:''
//             })
//         }else{
//             var address = '',goodsList = [];

//             //创建订单Id
//             var platform = '622'; // 平台系统架构码
//             var r1 = Math.floor(Math.random()*10);
//             var r2 = Math.floor(Math.random()*10);

//             var sysDate = new Date().Format('yyyyMMddhhmmss');  // 系统时间：年月日时分秒
//             var orderId = platform+r1+sysDate+r2;  // 21位

//             // 订单创建时间
//             var createDate = new Date().Format('yyyy-MM-dd hh:mm:ss');

//             // 生成订单
//             var order = {
//                 orderId:orderId,           // 订单id
//                 orderTotal:orderTotal,     // 订单总金额(直接拿前端传过来的参数)
//                 addressId:addressId,       // 地址信息
//                 goodId:goodId,            // 购买的商品信息
//                 goodNum: goodNum,
//                 orderStatus:'1',           // 订单状态，1成功
//                 createDate:createDate      // 订单创建时间
//             }

//             // 订单信息存储到数据库
//             doc.orderList.push(order);

//             doc.save(function (err1,doc1) {
//                 if(err1){
//                     res.json({
//                         status:"1",
//                         msg:err.message,
//                         result:''
//                     });
//                 }else{
//                     // 返回订单的id和订单的总金额给前端，下一个页面要用到
//                     res.json({
//                         status:"0",
//                         msg:'',
//                         result:{
//                             orderId:order.orderId,
//                             orderTotal:order.orderTotal
//                         }
//                     });
//                 }
//             });
//         }
//     })
// })

// 订单成功页面 **********************************************
//根据订单Id查询订单信息
router.get("/orderDetail", function (req, res, next) {
    var userId = req.cookies.userId,
        orderId = req.param("orderId");   // 前端传过来的订单id
    User.findOne({ "_id": userId }, function (err, userInfo) {
        if (err) {
            res.json({
                status: '1',
                msg: err.message,
                result: ''
            });
        } else {
            var orderList = userInfo.orderList;  // orderList订单列表
            if (orderList.length > 0) {  // 说明有订单
                var orderTotal = 0;
                // 遍历订单列表，根据订单id得到该订单总金额orderTotal
                orderList.forEach((item) => {
                    if (item.orderId == orderId) {
                        orderTotal = item.orderTotal;
                    }
                });
                if (orderTotal > 0) {
                    res.json({
                        status: '0',
                        msg: '',
                        result: {
                            orderId: orderId,
                            orderTotal: orderTotal
                        }
                    })
                } else {
                    res.json({
                        status: '120002',
                        msg: '无此订单',
                        result: ''
                    });
                }
            } else {
                res.json({
                    status: '120001',
                    msg: '当前用户未创建订单',
                    result: ''
                });
            }
        }
    })
});


router.get('/getUserData', function (req, res, next) {
    var userId = req.cookies.userId;   // 获取用户Id
    User.findOne({ "_id": userId }, function (err, doc) {
        if (err) {
            res.json({
                status: '1',
                msg: err.message,
                result: ''
            });
        } else {
            if (doc) {
                res.json({
                    status: '0',
                    msg: '',
                    result: doc
                })
            }
        }
    })
})

router.post("/updateBodyProfiles", function (req, res, next) {
    var userId = req.cookies.userId;
    var key1 = req.body.key1;
    var value1 = isNaN(req.body.value1) ? 0 : req.body.value1;
    var key2 = req.body.key2;
    var value2 = isNaN(req.body.value2) ? 0 : req.body.value2;
    var key3 = req.body.key3;
    var value3 = isNaN(req.body.value3) ? 0 : req.body.value3;

    User.findOne({ "_id": userId }, function (err, doc) {
        if (err) {
            res.json({
                status: '1',
                msg: err.message,
                result: ''
            });
        } else {
            if (doc) {
                if (!doc.profiles) {
                    doc.profiles = {};
                }
                if (key1 && String(key1).length > 0) {
                    doc.profiles[String(key1)] = value1
                }
                if (key2 && String(key2).length > 0) {
                    doc.profiles[String(key2)] = value2
                }
                if (key3 && String(key3).length > 0) {
                    doc.profiles[String(key3)] = value3
                }

                doc.save(function (err1, userInfo1) {
                    if (err1) {
                        res.json({
                            status: "1",
                            msg: err,
                            result: ''
                        });
                    } else {
                        res.json({
                            status: "0",
                            msg: '',
                            result: userInfo1
                        });
                    }
                });
            }
        }
    })
});

module.exports = router;

function addScores(userId, num, res) {

    User.findOne({ _id: userId }, function (err, existingUser) {

        if (err) { return err; }
        if (existingUser) {
            existingUser.scores = (parseInt(existingUser.scores) || 0) + num;
            existingUser.save(function (err1, doc1) {
                if (err1) { return err1; }
                if (doc1 && res) {
                    res.json({
                        status: '0',
                        msg: '',
                        result: doc1
                    });
                }
            });
        } else {
            res.json({
                status: '1',
                msg: ''
            });
        }
    });
}

function randomCode(strLength) {
    var result = [];
    strLength = strLength || 4;
    var charSet = "0123456789";
    while (strLength--) {
        result.push(charSet.charAt(Math.floor(Math.random() * charSet.length)));
    }
    return result.join("");
}
function randomString(strLength) {
    var result = [];
    strLength = strLength || 5;
    var charSet = "0123456789abcdefghijklmnopqrstuvwxyz";
    while (strLength--) {
        result.push(charSet.charAt(Math.floor(Math.random() * charSet.length)));
    }
    return result.join("");
}

const SMSClient = require('@alicloud/sms-sdk')
// ACCESS_KEY_ID/ACCESS_KEY_SECRET 根据实际申请的账号信息进行替换
const accessKeyId = 'LTAImNWx6Q6lCP7W'
const secretAccessKey = 'NLBONh6h8V9eExerHFAz5hlyL2wHKb'
//初始化sms_client
var smsClient = new SMSClient({ accessKeyId, secretAccessKey })
//发送短信
function sendSMS(phone, code, res) {
    smsClient.sendSMS({
        PhoneNumbers: phone,
        SignName: '氢泡泡',
        TemplateCode: 'SMS_137800215',
        TemplateParam: '{"code":' + code + '}'
    }).then(function (smsres) {
        let {Code} = smsres
        if (Code === 'OK') {
            //处理返回参数
            // console.log("res:" + smsres)
            res.json({
                status: '0',
                msg: ''
            });
        } else {
            res.json({
                status: '1',
                msg: ""
            });
        }
    }, function (err) {
        console.log(err.message)
        res.json({
            status: '1',
            msg: err.message
        });
    })
}


function checkCode(phone, v_code, res) {
    if (!phoneCodes[phone]) {
        res.json({
            status: "424",
            msg: '验证码错误.'
        });
        return false;
    }
    var sendTime = phoneCodes[phone]["time"];
    if (new Date().getTime() - sendTime > 600 * 1000) {
        res.json({
            status: "425",
            msg: '验证码失效.'
        });
        return false;
    }
    var code = phoneCodes[phone]["code"];
    if (code != v_code) {
        res.json({
            status: "426",
            msg: '验证码不正确.'
        });
        return false;
    }
    return true;
}



