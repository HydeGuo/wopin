var express = require('express');
var router = express.Router(); // 拿到express框架的路由
var mongoose = require('mongoose');
var User = require('./../models/users.js').UserSchema;
var Goods = require('../models/goods').Produce;
var ExchangeGoods = require('../models/goods').ExchangeProduce;
var ExchangeOrder = require('./../models/orders').ExchangeOrder;
var ScoresOrder = require('./../models/orders').ScoresOrder;
var CrowdfundingOrder = require('./../models/orders').CrowdfundingOrder;



router.post('/payMentExchange', function (req, res, next) {
	// 前端传参：订单的地址id;订单最终的总金额
	var userId = req.cookies.userId,
		addressId = req.body.addressId,
		goodsId = req.body.goodsId,
		title = req.body.title,
		image = req.body.image,
		num = req.body.num,
		offerPrice = req.body.offerPrice,
		singlePrice = req.body.singlePrice
	User.findOne({ "_id": userId }, function (err, doc) {
		if (err) {
			res.json({
				status: '1',
				msg: err.message,
				result: ''
			})
		} else {
			var address = '', goodsList = [];

			//创建订单Id
			var platform = '622'; // 平台系统架构码
			var r1 = Math.floor(Math.random() * 10);
			var r2 = Math.floor(Math.random() * 10);

			var sysDate = new Date().Format('yyyyMMddhhmmss');  // 系统时间：年月日时分秒
			var orderId = platform + r1 + sysDate + r2;  // 21位

			doc.addressList.forEach((item) => {
				if (addressId == item.addressId) {
					address = item;
				}
			})

			var createDate = new Date().Format('yyyy-MM-dd hh:mm:ss');
			var order = new ExchangeOrder({
				orderId: orderId,           // 订单id
				userId: userId,
				title: title,
				image: image,
				goodsId: goodsId,
				address: address,
				num: num,
				offerPrice: offerPrice,
				singlePrice: singlePrice,
				orderStatus: '待发货',
				createDate: createDate
			});
			order.save(function (err_order, doc_order) {
				if (!err_order) {
					// doc.exchangeOrderList.push(orderId);
					doc.save(function (err1, doc1) {
						if (err1) {
							res.json({
								status: "1",
								msg: err.message,
								result: ''
							});
						} else {
							res.json({
								status: "0",
								msg: '',
								result: ""
							});
						}
					});
				}
			});

		}
	})
})

router.post("/exchangeOrderList", function (req, res, next) {
	var userId = req.cookies.userId;
	var params = {}
	if (req.body.orderStatus) {
		params["orderStatus"] = req.body.orderStatus;
	}
	if (req.body.orderId) {
		params["orderId"] = req.body.orderId;
	}

	User.findOne({ "_id": userId }, function (err, doc) {
		if (err) {
			res.json({
				status: '1',
				msg: err.message,
				result: ''
			});
		} else {
			if (doc && doc.role == "admin") {
				ExchangeOrder.find(params, function (err, doc1) {
					if (err) {
						res.json({
							status: '1',
							msg: err.message,
							result: ''
						});
					} else {
						if (doc1) {
							res.json({
								status: '0',
								msg: '',
								result: doc1
							})
						}
					}
				})
			}
			else {
				res.json({
					status: '1',
					msg: "非法访问",
					result: ''
				});
			}
		}
	})
});


router.post("/exchangeOrderUpdate", function (req, res, next) {
	var orderId = req.body.orderId,
		expressSendId = req.body.expressId;

	var userId = req.cookies.userId;
	User.findOne({ "_id": userId, "role": "admin" }, function (err, doc) {
		if (err) {
			res.json({
				status: '1',
				msg: err.message,
				result: ''
			});
		} else {
			if (doc) {
				ExchangeOrder.updateOne({ "orderId": orderId }, { $set: { "expressSendId": expressSendId, "orderStatus": "已发货" } }, function (err, updateOrder) {
					if (err) { return next(err); }
					ExchangeOrder.findOne({ "orderId": orderId }, function (err, existingOrder) {
						if (err) { return next(err); }
						res.json({
							status: "0",
							msg: '',
							result: existingOrder
						})
					});
				});
			}
			else {
				res.json({
					status: '1',
					msg: "非法访问",
					result: ''
				});
			}
		}
	})
});

router.post("/scoresOrderList", function (req, res, next) {
	var userId = req.cookies.userId;
	var params = {}
	if (req.body.orderStatus) {
		params["orderStatus"] = req.body.orderStatus;
	}
	if (req.body.orderId) {
		params["orderId"] = req.body.orderId;
	}

	User.findOne({ "_id": userId }, function (err, doc) {
		if (err) {
			res.json({
				status: '1',
				msg: err.message,
				result: ''
			});
		} else {
			if (doc && doc.role == "admin") {
				ScoresOrder.find(params, function (err, doc1) {
					if (err) {
						res.json({
							status: '1',
							msg: err.message,
							result: ''
						});
					} else {
						if (doc1) {
							res.json({
								status: '0',
								msg: '',
								result: doc1
							})
						}
					}
				})
			}
			else {
				res.json({
					status: '1',
					msg: "非法访问",
					result: ''
				});
			}
		}
	})
});


router.post("/scoresOrderUpdate", function (req, res, next) {
	var orderId = req.body.orderId,
		expressSendId = req.body.expressId;

	var userId = req.cookies.userId;
	User.findOne({ "_id": userId, "role": "admin" }, function (err, doc) {
		if (err) {
			res.json({
				status: '1',
				msg: err.message,
				result: ''
			});
		} else {
			if (doc) {
				ScoresOrder.updateOne({ "orderId": orderId }, { $set: { "expressSendId": expressSendId, "orderStatus": "已发货" } }, function (err, updateOrder) {
					if (err) { return next(err); }
					ScoresOrder.findOne({ "orderId": orderId }, function (err, existingOrder) {
						if (err) { return next(err); }
						res.json({
							status: "0",
							msg: '',
							result: existingOrder
						})
					});
				});
			}
			else {
				res.json({
					status: '1',
					msg: "非法访问",
					result: ''
				});
			}
		}
	})
});

router.post("/crowdfundingOrderList", function (req, res, next) {
	var userId = req.cookies.userId;
	var params = {}
	if (req.body.orderStatus) {
		params["orderStatus"] = req.body.orderStatus;
	}
	if (req.body.orderId) {
		params["orderId"] = req.body.orderId;
	}

	User.findOne({ "_id": userId }, function (err, doc) {
		if (err) {
			res.json({
				status: '1',
				msg: err.message,
				result: ''
			});
		} else {
			if (doc && doc.role == "admin") {
				CrowdfundingOrder.find(params, function (err, doc1) {
					if (err) {
						res.json({
							status: '1',
							msg: err.message,
							result: ''
						});
					} else {
						if (doc1) {
							res.json({
								status: '0',
								msg: '',
								result: doc1
							})
						}
					}
				})
			}
			else {
				res.json({
					status: '1',
					msg: "非法访问",
					result: ''
				});
			}
		}
	})
});


router.post("/crowdfundingOrderUpdate", function (req, res, next) {
	var orderId = req.body.orderId,
		expressSendId = req.body.expressId;

	var userId = req.cookies.userId;
	User.findOne({ "_id": userId, "role": "admin" }, function (err, doc) {
		if (err) {
			res.json({
				status: '1',
				msg: err.message,
				result: ''
			});
		} else {
			if (doc) {
				CrowdfundingOrder.updateOne({ "orderId": orderId }, { $set: { "expressSendId": expressSendId, "orderStatus": "已发货" } }, function (err, updateOrder) {
					if (err) { return next(err); }
					CrowdfundingOrder.findOne({ "orderId": orderId }, function (err, existingOrder) {
						if (err) { return next(err); }
						res.json({
							status: "0",
							msg: '',
							result: existingOrder
						})
					});
				});
			}
			else {
				res.json({
					status: '1',
					msg: "非法访问",
					result: ''
				});
			}
		}
	})
});
module.exports = router;

