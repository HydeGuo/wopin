var express = require('express');
var router = express.Router();
var Location = require('./../models/devices').Location;
var UUID = require('../models/uuids').UUID;
var GoodsRouter = require('./goods');
var fs = require('fs');
var url = require('url');
const requestIp = require('request-ip');

var _appId = "wxf42ec50449767feb";
var _mchId = "1513314731";
var _weChatKey = "50DK15GO1aGbESPQjBI1Wqubnq6ylFMS";

var WechatPayment = require('wechat-payment-node').default;
// import WechatPayment from 'wechat-payment-node';
let options = {
  appid: _appId,
  mch_id: _mchId,
  apiKey: _weChatKey, //微信商户平台API密钥
  notify_url: "http://wifi.h2popo.com:8081/goods/wechatpay",
  trade_type: 'APP',
  pfx: fs.readFileSync("./apiclient_cert.p12")
};
let wechatPaymentInstance = new WechatPayment(options);


/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', { title: 'Express' });
});


router.post('/location', function (req, res, next) {
  var param = {
    device_id: req.body.device_id,
    time: req.body.time,
    lat: req.body.lat,
    long: req.body.long,
    link: req.body.link
  }

  if (!param.device_id || !param.time) {
    res.json({
      status: "500",
      msg: 'device_id and time required',
      result: null
    })
    return
  }
  // console.log(`device_id:${req.body.device_id},time:${req.body.time},device_id:${param.device_id}`);
  var location = new Location({
    device_id: param.device_id,
    time: param.time,
    lat: param.lat,
    long: param.long,
    link: param.link
  });
  location.save(function (err, loca) {
    if (err) { return next(err); }
    res.json({
      status: "0",
      msg: '',
      result: ''
    })
  });

});


router.get('/getLocations', function (req, res, next) {
  var param = {
    device_id: req.query.device_id
  }
  if (!param.device_id) {
    res.json({
      status: "404",
      msg: '',
      result: null
    })
    return
  }
  // console.log(`device_id:${param.device_id}`);
  Location.find(param, function (err, doc) {  // 根据用户名密码查找数据库
    if (err) {
      res.json({
        status: "1",
        msg: err.message
      })
    } else {
      if (doc) {
        res.json({
          status: "0",
          msg: '',
          result: doc
        })
      } else {
        res.json({
          status: "404",
          msg: '',
          result: null
        })
      }
    }
  })
});



router.post('/getWeChatPaySign', function (req, res, next) {

  const clientIp = requestIp.getClientIp(req).split(':').pop();;

  var body = req.body.body;
  var attach = req.body.attach;
  var amount = 1;//req.body.amount;
  var orderId = req.body.orderId;

  console.log("getWeChatPaySign orderId:"+orderId);
  let orderData = {
    body: body, // 商品或支付单简要描述
    attach: attach,
    out_trade_no: orderId, // 商户系统内部的订单号,32个字符内、可包含字母
    total_fee: amount,
    spbill_create_ip: clientIp,
    notify_url: "http://wifi.h2popo.com:8081/goods/wechatpay",
    trade_type: 'APP'
  };

  wechatPaymentInstance.createUnifiedOrder(orderData)
    .then(result => {
      console.log(result);
      var prepayId = result["prepay_id"];
      var nonceStr = result["nonce_str"];
      var timeStamp = parseInt((new Date().getTime()) / 1000);

      var stringSignTemp = "appid=" + _appId + "&noncestr=" + nonceStr + "&package=Sign=WXPay&partnerid=" + _mchId + "&prepayid=" + prepayId + "&timestamp=" + timeStamp + "&key=" + _weChatKey;

      var crypto = require('crypto');
      var md5 = crypto.createHash('md5');
      var sign = md5.update(stringSignTemp, 'utf8').digest('hex').toUpperCase()

      GoodsRouter.setPreOrderId(prepayId, orderId);

      var doc = {
        "partnerid": "1513314731",
        "package": "Sign=WXPay",
        "timestamp": timeStamp,
        "prepayid": prepayId,
        "noncestr": nonceStr,
        "sign": sign
      }
      res.json({
        status: "0",
        msg: "",
        result: doc
      })
    })
    .catch(err => {
      console.log("getWeChatPaySign.err:" + err);
      res.json({
        status: "1",
        msg: JSON.stringify(err)
      })
    });

});


function randomString(strLength) {
  var result = [];
  strLength = strLength || 5;
  var charSet = "0123456789abcdefghijklmnopqrstuvwxyz";
  while (strLength--) {
    result.push(charSet.charAt(Math.floor(Math.random() * charSet.length)));
  }
  return result.join("");
}


router.post('/UUIDs', function (req, res, next) {

  // var key = req.body.key;
  // if (key == '6c1ae2db6e23d5a2e9e86e2294c26be7') {
  //   UUID.find({}, function (err, doc) {  // 根据用户名密码查找数据库
  //     if (err) {
  //       res.json({
  //         status: "1",
  //         msg: err.message
  //       })
  //     } else {
  //       if (doc) {
  //         res.json({
  //           count: doc.length,
  //           result: doc
  //         })
  //       } else {
  //         res.json({
  //           count: 0,
  //           result: []
  //         })
  //       }
  //     }
  //   })
  // } else {
  //   res.json({
  //     status: "401"
  //   })
  // }
});
router.post('/generateUUID', function (req, res, next) {
  // var num = req.body.num;
  // var key = req.body.key;
  // if (key == '6c1ae2db6e23d5a2e9e86e2294c26be7') {
  //   var uuidList = [];
  //   for (var i = 0; i < num; i++) {
  //     var new_uuid = generateUUID();
  //     // console.log("uuid:" + new_uuid);
  //     uuidList.push(new_uuid);
  //     var uuid = new UUID({
  //       "uuid": new_uuid
  //     });
  //     uuid.save();
  //   }
  //   res.json(uuidList)
  // } else {
  //   res.json({
  //     status: "401"
  //   })
  // }
});

function generateUUID() {
  var d = new Date().getTime();
  var uuid = 'xxxx-xxxx-xxxx-yxxx-xxxx'.replace(/[xy]/g, function (c) {
    var r = (d + Math.random() * 16) % 16 | 0;
    d = Math.floor(d / 16);
    return (c == 'x' ? r : (r & 0x7 | 0x8)).toString(16);
  });
  return uuid;
}

module.exports = router;




