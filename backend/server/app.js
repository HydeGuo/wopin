var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var ejs = require('ejs');

var index = require('./routes/index');
var users = require('./routes/users');
var goods = require('./routes/goods');
var admin = require('./routes/admin');
var media = require('./routes/media');
var blog = require('./routes/blog');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.engine('.html', ejs.__express);  // 设置html后缀
app.set('view engine', 'html');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));


// 捕获登录状态
app.use(function (req, res, next) {   // 进入路由之前优先进入function
    if (req.cookies.userId) {  // 有cookies,说明已经登录
        next();
    } else {
        console.log(`path:${req.path},originalUrl:${req.originalUrl}`);

        // 结果例 => path:/goods/list,originalUrl:/goods/list?page=1&pageSize=8&sort=1&priceLevel=all
        if (req.originalUrl == '/users/login' ||
            req.originalUrl == '/users/register' ||
            req.originalUrl == '/users/emailLogin' ||
            req.originalUrl == '/users/emailRegister' ||
            req.originalUrl == '/users/resetEmailPassword' ||
            req.originalUrl == '/users/changePassword' ||
            req.originalUrl == '/users/thirdLogin' ||
            req.originalUrl == '/users/thirdRegister' ||
            req.originalUrl == '/users/sendVerifyCode' ||
            req.originalUrl == '/users/logout' ||
            req.originalUrl == '/users/drink' ||
            req.originalUrl == '/users/changePasswordByToken' ||
            req.originalUrl.indexOf('/images') > -1 ||
            req.originalUrl.indexOf('/uploadImage') > -1 ||
            // req.originalUrl.indexOf('/crowdfundingOrderTotalMoney')>-1 ||
            req.originalUrl.indexOf('/getLocations') > -1 ||
            req.originalUrl == '/goods/wechatpay' ||
            req.originalUrl == '/location' ||
            req.originalUrl == '/generateUUID' ||
            req.originalUrl == '/UUIDs' ||
            req.originalUrl.indexOf('/goods/list') > -1) {  // 未登录时可以点击登录login登出logout和查看商品列表
            // if(req.originalUrl =='/users/login' || req.originalUrl == '/users/logout' || req.path == '/goods/list'){   // 第二种方法
            next();
        } else {
            if (req.originalUrl.indexOf('/downloadApp') > -1 ) {
                var rUrl = "";

                var ua = req.headers['user-agent'],
                    $ = {};

                if (/mobile/i.test(ua))
                    $.Mobile = true;

                if (/like Mac OS X/.test(ua)) {
                    rUrl = 'https://itunes.apple.com/cn/app/%E6%B0%A2%E6%B3%A1%E6%B3%A1/id1194023418?mt=8';
                }

                if (/Android/.test(ua)){
                    rUrl = 'http://download.h2popo.com/wopin-res/app/h2popo.apk';
                }
                
                res.redirect(rUrl);　
                res.end();
            } else {
                res.json({
                    status: '1001',
                    msg: '当前未登录',
                    result: ''
                })
            }
        }
    }
})


// 一级路由
app.use('/', index);
app.use('/users', users);
app.use('/goods', goods.router);
app.use('/admin', admin);
app.use('/blog', blog);
app.use('/images', media);
app.use('/uploadImage', media);

// catch 404 and forward to error handler 捕获404的
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    console.log(err.message);
    next(err);
});

// error handler  捕获500状态的
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    console.log(err.message);
    res.status(err.status || 500);
    res.render('error');
});

module.exports = app;

