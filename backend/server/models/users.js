// 对应数据库用户数据在resource文件夹的dumall-users
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var cup = new Schema({
    "type": String,
    "name": String,
    "uuid": String
});
var address = new Schema({
    "addressId": String,
    "userName": String,
    "address1": String,
    "address2": String,
    "postCode": Number,
    "tel": Number,
    "isDefault": Boolean
});
// 定义一个Schema
var userSchema = new Schema({
    // 'userId':String,   // 或者 'userId':{type:String}
    'userName': String,
    'userPwd': String,
    'icon': String,
    'role': String,
    'level': Number,
    "phone": Number,
    'email': String,
    // 'exchangeOrderList':[String],
    // 'creditsOrderList':[String],
    // 'crowdfundingOrderList':[String],
    "drinks": Number,
    "scores": Number,
    "lastAttendance":String,
    "followList": [String],
    "fansList": [String],
    'profiles': {
        "height": Number,
        "weight": Number,
        "age": Number,
        "blood_sugar_full": Number,
        "blood_sugar_hugry": Number,
        "blood_lipid_all": Number,
        "blood_lipid": Number,
        "blood_lipid_TG": Number,
        "blood_pressure": Number,
        "blood_pressure_press": Number
    },
    'cupList': [cup],
    'cartList': [           // 购物车列表
        {
            "productId": String,
            "productNum": Number  // 商品数量
        }
    ],
    "addressList": [address]
}, {
        usePushEach: true
    })

// 输出(导出)
// module.exports = mongoose.model('user', userSchema); 
var UserSchema = mongoose.model('user', userSchema); 
exports.UserSchema = UserSchema;



var emailReset = new Schema({
	'email':String,
	'token':String
})

var EmailReset = mongoose.model('emailReset',emailReset);
exports.EmailReset = EmailReset;









