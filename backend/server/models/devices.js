
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var drinkLocationSchema = new Schema({
 device_id: String,
 time: String,
 lat: Number, 
 long: Number, 
 link: String
})

var Location = mongoose.model('location', drinkLocationSchema);

exports.Location = Location;




var devices = new Schema({
	'uuid':String,   
	'type':String,  
	'name':String,   
	'address':String,   
	'color':Number,
	'firstRegisterTime':String,
	'registerTime':String,
	'userId':String,
	'produceScores':Number
})

var Devices = mongoose.model('devices',devices); 

exports.Devices = Devices;

