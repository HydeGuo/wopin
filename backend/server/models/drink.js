
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var drinkItemSchema = new Schema({
	'time': String
})

var drinkSchema = new Schema({
	'userId': String,
	'target': Number,
	"date": String,
	'drinks': [drinkItemSchema]
}, {
	usePushEach: true
})

var Drink = mongoose.model('drink', drinkSchema);

exports.Drink = Drink;







