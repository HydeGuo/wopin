// 对应数据库商品列表数据在resource文件夹的dumall-goods
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


// 定义一个Schema
var blogSchema = new Schema({
	'id': Number,
	'userId': String,
	'userName': String,
	'likes': [String],
	'comments': Number,
	'collect': [String],
	'read': Number
}, {
		usePushEach: true
	})

// 输出(导出)
exports.Post = mongoose.model('post', blogSchema);


var commentSchema = new Schema({
	'id': Number,
	'postId': Number,
	'userId': String,
	'parent': Number,
	'likes': [String]
}, {
		usePushEach: true
	})

exports.Comment = mongoose.model('comment', commentSchema);

var historySchema = new Schema({
	'userId': String,
	'history': [Number]
}, {
		usePushEach: true
	})

exports.History = mongoose.model('history', historySchema);


var followSchema = new Schema({
	'userId': String,
	'follow': [String]
}, {
		usePushEach: true
	})

exports.Follow = mongoose.model('follow', followSchema);


var commentSchema = new Schema({

	"id": Number,
	"post": Number,
	"parent": Number,
	"author": Number,
	"author_name": String,
	"avatar_URL": String,
	"content": String,
	"date": String,
	"type": String,
	"postTitle": String,
	"postThumbnail": String
})

var getMsgSchema = new Schema({
	'userId': String,
	'newComment': [commentSchema],
	'time': Number
}, {
		usePushEach: true
	})
exports.BlogMsg = mongoose.model('getMsg', getMsgSchema);


var sysMsgSchema = new Schema({
	'userId': String,
	'time': Number
})
exports.SysMsg = mongoose.model('sysMsg', sysMsgSchema); 
