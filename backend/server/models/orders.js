// 对应数据库商品列表数据在resource文件夹的dumall-goods
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

require('./goods.js');

// 定义一个Schema
var ordersSchema = new Schema({
	'userId':String,
	'goodsId':String,  
	'addressId':String,
	'salePrice':Number,
    "productNum":Number,
})

// 输出(导出)
var  Order = mongoose.model('orders',ordersSchema); 

var exchangeOrderSchema = new Schema({
	'orderId':String,
	'userId':String,
	'title':String,
	'image':String,
	'goodsId':Number,  
	'num':Number,  
	'address':Object,
    "offerPrice":Number,
    "oldGoodName":String,
    "singlePrice":Number,
	'orderStatus':String,
	'expressReturnId':String,
	'expressReturnName':String,
	'expressSendId':String,
	'orderStatus':String,
	'createDate':String,
	'createTime':Number,
	'infoUserName':String,
	'infoSex':String,
	'infoPhone':String,
	'infoCupModel':String,
	'infoCupColor':String,
	'infoBuyTime':String,
	'infoUsage':String
})

var  ExchangeOrder = mongoose.model('exchange_orders',exchangeOrderSchema); 


exports.Order = Order;
exports.ExchangeOrder = ExchangeOrder;





var scoresOrderSchema = new Schema({
	'orderId':String,
	'userId':String,
	'title':String,
	'image':String,
	'goodsId':Number,  
	'num':Number,  
	'address':Object,
    "singlePrice":Number,
	'orderStatus':String,
	'expressSendId':String,
	'orderStatus':String,
	'createDate':String,
	'createTime':Number
})

var  ScoresOrder = mongoose.model('scores_orders',scoresOrderSchema); 
exports.ScoresOrder = ScoresOrder;


var crowdfundingOrderSchema = new Schema({
	'orderId':String,
	'userId':String,
	'title':String,
	'image':String,
	'goodsId':Number,  
	'num':Number,  
	'address':Object,
    "singlePrice":Number,
	'orderStatus':String,
	'expressSendId':String,
	'orderStatus':String,
	'createDate':String,
	'createTime':Number
})

var  CrowdfundingOrder = mongoose.model('crowdfunding_orders',crowdfundingOrderSchema); 
exports.CrowdfundingOrder = CrowdfundingOrder;
