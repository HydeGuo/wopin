import Vue from 'vue'
import Router from 'vue-router'

// import GoodsList from '@/views/GoodsList'  // 商品列表
// import Cart from '@/views/Cart'  // 购物车列表
// import Address from '@/views/Address'  // 地址列表
// import OrderConfirm from '@/views/OrderConfirm'  // 订单确认页面
// import OrderSuccess from '@/views/OrderSuccess'  //  订单成功页面
import ExchangeOrderList from '@/views/ExchangeOrderList'  
import ExchangeOrderDetail from '@/views/ExchangeOrderDetail'  
import ScoresOrderList from '@/views/ScoresOrderList'  
import ScoresOrderDetail from '@/views/ScoresOrderDetail'  
import CrowdfoudingOrderList from '@/views/CrowdfoudingOrderList'  
import CrowdfoudingOrderDetail from '@/views/CrowdfoudingOrderDetail'  

Vue.use(Router)

export default new Router({
  routes: [
    // {
    //   path: '/',
    //   name: 'GoodsList',
    //   component: GoodsList
    // },
    {
      path: '/',
      name: 'ExchangeOrderList',
      component: ExchangeOrderList 
    },
    {
      path: '/exchangeOrderDetail',
      name: 'ExchangeOrderDetail',
      component: ExchangeOrderDetail 
    },
    {
      path: '/scoresOrderList',  
      name: 'ScoresOrderList',
      component: ScoresOrderList => require(['@/views/ScoresOrderList'],ScoresOrderList)
    },
    {
      path: '/scoresOrderDetail',  
      name: 'ScoresOrderDetail',
      component: ScoresOrderDetail => require(['@/views/ScoresOrderDetail'],ScoresOrderDetail)
    },
    {
      path: '/crowdfoudingOrderList',  
      name: 'CrowdfoudingOrderList',
      component: CrowdfoudingOrderList => require(['@/views/CrowdfoudingOrderList'],CrowdfoudingOrderList)
    },
    {
      path: '/crowdfoudingOrderDetail',  
      name: 'CrowdfoudingOrderDetail',
      component: CrowdfoudingOrderDetail => require(['@/views/CrowdfoudingOrderDetail'],CrowdfoudingOrderDetail)
    },
    {
      path: '/address',   // 地址列表路由
      name: 'Address',
      component: Address => require(['@/views/Address'],Address)
    },
    {
      path: '/resetPassword',   // 地址列表路由
      name: 'ResetPassword',
      component: ResetPassword => require(['@/views/ResetPassword'],ResetPassword)
    },
    // {
    //   path: '/OrderList',   
    //   name: 'OrderList',
    //   component: OrderList
    // }
  ]
})
