

> A Vue.js project

### 运行项目

1、先安装mongodb和环境搭建： [http://www.cnblogs.com/ccyinghua/p/7887713.html](http://www.cnblogs.com/ccyinghua/p/7887713.html)

2、安装mongovue,建立dumall数据库，增加goods和users集合，插入数据(数据在resource/dumall-goods和resource/dumall-users) <br>
![image](https://github.com/ccyinghua/vue-node-mongodb-project/blob/master/resource/readme/1.jpg?raw=true)

3、npm install<br>
4、node server/bin/www  // 启动express后端服务<br>
5、npm run dev


### 构建项目

```javascript
vue init webpack vue-node-mongodb-project

cnpm install
npm run dev

cnpm install vue-resource --save
cnpm install axios --save

cnpm install vue-lazyload --save   // 图片加载

* 构建express的一些安装 // 参考 02-express.md
* 安装mongoose // 参考03-GoodsListInterface.md

* cnpm install vue-infinite-scroll --save  // 安装滚动加载插件  04-pagingAndSort.md

* cnpm install vuex --save  // 安装vuex  10-vuex.md

```
