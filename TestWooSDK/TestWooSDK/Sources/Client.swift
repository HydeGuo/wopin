import Alamofire
import Foundation
import ObjectMapper

public class Client {
    public static let sharedClient = Client()

    public var consumerKey: String?
    public var consumerSecret: String?
    public var siteURL: String?

    init() {}

    init(siteURL: String, consumerKey: String, consumerSecret: String) {
        self.siteURL = siteURL
        self.consumerKey = consumerKey
        self.consumerSecret = consumerSecret
    }

    public func get<T: Mappable>(type: String, id: Int, completion: @escaping (_ success: Bool, _ value: T?) -> Void) {
        let baseURL = URL(string: siteURL!)
        let requestURL = URL(fileURLWithPath: "wc-api/v3/\(type)s/\(id)", relativeTo: baseURL)
        let requestURLString = requestURL.absoluteString

        guard let user = consumerKey, let password = consumerSecret else {
            completion(false, nil)
            return
        }
        
        
        print(".....\(user)......\(password)")

        Alamofire.request(requestURLString).authenticate(user: user, password: password).responseJSON{
            response in
            if response.result.isSuccess {
                print("......1.....\(response.result.value)")
                let object = Mapper<T>().map(JSONString: (response.result.value as! [String : String])[type]!)//map(response.result.value![type])
                completion(true, object)
            } else {
                print("......error.....\(response.error)")
                completion(false, nil)
            }
        }
        
        
    }

    public func getArray<T: Mappable>(type: RequestType, slug: String, limit: Int = 10, completion: @escaping (_ success: Bool, _ value: [T]?) -> Void) {
        guard let url = siteURL, let user = consumerKey, let password = consumerSecret else {
            completion(false, nil)
            return
        }
        let subStr = slug.contains("?") ? slug + "&" : slug + "?";
        let baseURL = URL(string: url+"/wp-json/wc/v2/\(subStr)consumer_key=\(user)&consumer_secret=\(password)")
        Alamofire.request((baseURL?.absoluteString)!).responseJSON{
            response in
            if response.result.isSuccess {
                print(".....getArray.....\(response.result.value!)")
                
                let objects = Mapper<T>().mapArray(JSONfile: (response.result.value! as! [String : String])[type.rawValue]!)
                completion(true, objects)
            } else {
                completion(false, nil)
            }
        }
//        Alamofire.request(.GET, requestURLString)
//            .authenticate(user: user, password: password)
//            .responseJSON { response in
//                if response.result.isSuccess {
//                    let objects = Mapper<T>().mapArray(response.result.value![type.rawValue])!
//                    completion(success: true, value: objects)
//                } else {
//                    completion(success: false, value: nil)
//                }
//        }
    }
}
